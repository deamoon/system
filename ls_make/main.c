#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>
enum
  {
    DT_UNKNOWN = 0,
# define DT_UNKNOWN	DT_UNKNOWN
    DT_FIFO = 1,
# define DT_FIFO	DT_FIFO
    DT_CHR = 2,
# define DT_CHR		DT_CHR
    DT_DIR = 4,
# define DT_DIR		DT_DIR
    DT_BLK = 6,
# define DT_BLK		DT_BLK
    DT_REG = 8,
# define DT_REG		DT_REG
    DT_LNK = 10,
# define DT_LNK		DT_LNK
    DT_SOCK = 12,
# define DT_SOCK	DT_SOCK
    DT_WHT = 14
# define DT_WHT		DT_WHT
  };
void ls(char * path) {
    DIR * dir = opendir(path);
    struct dirent * ent; int q = 0;
    while ((ent = readdir(dir)) != NULL) {
        if (ent->d_name[0]=='.') continue;
        printf("%s  ", ent->d_name); q = 1;
    }
    if (q) printf("\n");
    closedir(dir);
}

void ls_l(char * path) {
    //printf("%s",path);
    DIR * dir = opendir(path);
    struct dirent * ent;
    //struct stat s;
    //stat(path, &s);
    //printf("итого: %d %d %d\n",(int)s.st_size,(int)s.st_blksize,(int)s.st_blocks);
    /*struct stat s;
    stat(ent->d_name, &s);
    printf("итого: %d\n",(int)s2.st_size);*/
    // Добить итого и передавать полный путь в stat, а не только название
    while ((ent = readdir(dir)) != NULL) {
        struct stat s;


        int n1 = strlen(path), n2 = strlen(ent->d_name), i;
        char *str = malloc(sizeof(char) * (n1 + n2 + 2));
        for (i=0;i<n1;++i) str[i] = path[i];
        str[n1]='/';
        for (i=n1+1;i<n1+n2+1;++i) str[i] = ent->d_name[i-n1-1];
        str[n1+n2+1]='\0';
        stat(str, &s);
        free(str);


        struct passwd *p1; p1 = getpwuid(s.st_uid);
        struct group  *p2; p2 = getgrgid(s.st_gid);

        if (ent->d_name[0]=='.') continue;

        if (ent->d_type == DT_DIR) printf("d"); else printf("-");
        if (s.st_mode & S_IRUSR) printf("r"); else printf("-");
        if (s.st_mode & S_IWUSR) printf("w"); else printf("-");
        if (s.st_mode & S_IXUSR) printf("x"); else printf("-");
        if (s.st_mode & S_IRGRP) printf("r"); else printf("-");
        if (s.st_mode & S_IWGRP) printf("w"); else printf("-");
        if (s.st_mode & S_IXGRP) printf("x"); else printf("-");
        if (s.st_mode & S_IROTH) printf("r"); else printf("-");
        if (s.st_mode & S_IWOTH) printf("w"); else printf("-");
        if (s.st_mode & S_IXOTH) printf("x"); else printf("-");

        printf(" %d ", (int)s.st_nlink);
        if (p1) printf(" %s ", p1->pw_name); else printf("unknown ");
        if (p2) printf(" %s ", p2->gr_name); else printf("unknown ");
        printf("%7d",(int)s.st_size);
        time_t rawtime = s.st_mtime; struct tm *timeinfo;
        timeinfo = localtime(&rawtime);
        char * month[] = { "января", "февраля", "марта", "апреля", "мая",
                              "июня", "июля", "августа", "сентября", "октября",
                              "ноября","декабря"};
        printf(" %s %d %.2d:%.2d ", month[timeinfo->tm_mon], timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min);
        printf("%s\n",ent->d_name);
    }
    closedir(dir);
}

void ls_R(char * path, int q) {
    printf("%s:\n",path);
    if (q) ls(path); else ls_l(path);
    printf("\n");
    DIR *dir = opendir(path);

    struct dirent * ent;
    while ((ent = readdir(dir)) != NULL) {
        if (ent->d_type == DT_DIR) {
            if (ent->d_name[0]=='.') continue;
            int n1 = strlen(path), n2 = strlen(ent->d_name), i;
            char *s = malloc(sizeof(char) * (n1 + n2 + 2));
            for (i=0;i<n1;++i) s[i] = path[i];
            s[n1]='/';
            for (i=n1+1;i<n1+n2+1;++i) s[i] = ent->d_name[i-n1-1];
            s[n1+n2+1]='\0';
            ls_R(s,q);
            free(s);
        }
    }
    closedir(dir);
}

int main(int arg, char ** arv) {
            //DIR * dir = opendir(".");
            //ls(dir);
            //closedir(dir);

            //dir = opendir(".");
            ls_R(".",0);
            //closedir(dir);
//ls_R(".");
/*
    if (arg == 1) {
      DIR * dir = opendir(".");
      ls(dir);
      closedir(dir);
    } else if (arg == 2){
      if (arv[1][1] == 'l') {
        DIR * dir = opendir(".");
        ls_l(dir);
        closedir(dir);
      } else {
        //DIR * dir = opendir(".");
        ls_R(".");
        //closedir(dir);
      }
    }
*/
    return 0;
}

