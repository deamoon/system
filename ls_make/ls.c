#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>

void ls(DIR * dir) {
    struct dirent * ent;
    while ((ent = readdir(dir)) != NULL) {
        struct stat s;
        stat(ent->d_name, &s);
        printf("%s  ", ent->d_name);
    }
    printf("\n");
}

void ls_l(DIR * dir) {
    struct dirent * ent;
    while ((ent = readdir(dir)) != NULL) {
        struct stat s;
        stat(ent->d_name, &s);
        if (ent->d_type == DT_DIR) printf("d"); else printf("-");
        if (s.st_mode & S_IRUSR) printf("r"); else printf("-");
        if (s.st_mode & S_IWUSR) printf("w"); else printf("-");
        if (s.st_mode & S_IXUSR) printf("x"); else printf("-");
        if (s.st_mode & S_IRGRP) printf("r"); else printf("-");
        if (s.st_mode & S_IWGRP) printf("w"); else printf("-");
        if (s.st_mode & S_IXGRP) printf("x"); else printf("-");
        if (s.st_mode & S_IROTH) printf("r"); else printf("-");
        if (s.st_mode & S_IWOTH) printf("w"); else printf("-");
        if (s.st_mode & S_IXOTH) printf("x"); else printf("-");
        struct passwd *p1; p1 = getpwuid(s.st_uid);
        struct group  *p2; p2 = getgrgid(s.st_gid);
        printf(" %d %s %s ", (int)s.st_nlink, p1->pw_name , p2->gr_name);
        printf("%7d",(int)s.st_size);
        time_t rawtime = s.st_mtime; struct tm *timeinfo; 
        timeinfo = localtime(&rawtime);
        char * month[] = { "января", "февраля", "марта", "апреля", "мая", 
                              "июня", "июля", "августа", "сентября", "октября",
                              "ноября","декабря"};
        printf(" %s %d %.2d:%.2d ", month[timeinfo->tm_mon], timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min);
        printf("%s\n",ent->d_name);
    }

}

void ls_R(char * path) {
    printf("%s\n",path);
    DIR * dir = opendir(path); ls(dir); closedir(dir); dir = opendir(path);
    printf("\n");
    struct dirent * ent;
    while ((ent = readdir(dir)) != NULL) {
    //printf("ssssdefwef");
        if (ent->d_type == DT_DIR) {
            printf(" '%s' ",ent->d_name);
            if (ent->d_name[0]=='.') continue;
            ls_R(ent->d_name);
        }
    }
    closedir(dir);
//    printf("\n");    
}

int main(int arg, char ** arv) {
    
    if (arg == 1) {
      DIR * dir = opendir(".");
      ls(dir);
      closedir(dir);    
    } else if (arg == 2){
      if (arv[1][1] == 'l') {
        DIR * dir = opendir(".");
        ls_l(dir);
        closedir(dir);    
      } else {
        //DIR * dir = opendir(".");
        ls_R(".");
        //closedir(dir);    
      }  
    }
    
    return 0;
}


