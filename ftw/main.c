#include <stdio.h>
#include <ftw.h>
#include <stdlib.h>
#include <string.h>

char s[1000][100];
int n = -1;

int fn(const char *fpath, const struct stat *sb, int typeflag){
    if (typeflag == FTW_F) {
      //printf("%s -- %d \n",fpath,sb->st_size);
      strcpy(s[++n], fpath);
    }
    return 0;
}

int main(int argc, char *argv[]) {

//   cmpstringp(const void *p1, const void *p2)
  // qsort(&argv[1], argc - 1, sizeof(argv[1]), cmpstringp);

    if ( ftw ("/home/deamoon/Документы", fn, 20) ) {
        fprintf(stderr,"Error was");
        exit(1);
    }

    for (int i=0; i<n; ++i) printf("%c\n", s[i][0]);

    return 0;
}
