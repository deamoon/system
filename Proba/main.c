#include <stdio.h>
#include <stdlib.h>

do
{
	/* code */
} while (/* condition */);

int main(int argc, char const *argv[])
{
	
	return 0;
}

int c = 9;

void f(int **n){
    *n = &c;
}

int main(){
    int *n; int b = 5;
    n = &b;

    b = 7;
    f(&n);
    c = 90;
    printf("%d",*n);
    return 0;
}
