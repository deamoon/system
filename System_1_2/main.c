#include <stdio.h>
#include <stdlib.h>

typedef struct myy_string my_string;

struct myy_string{
    int length;
    char *s;
};

int bufer_vvod(my_string * s){
    s->s = (char *) malloc(sizeof(char)*(s->length));
    if (!s->s) {
        fprintf(stderr,"Don't acceess memory");
        return -1;
    }
    char c; int k = -1;
    while (1) {
        c = getchar();
        if (c=='\n') {
            s->s[++k] = '\0';
            break;
        } else
            s->s[++k] = c;
        if (k==s->length-1) {
            s->s = realloc(s->s, s->length*=2);
            if (!s->s) {
                fprintf(stderr,"Don't acceess memory");
                return -1;
            }
        }
    }
    s->length = k;
    return 0;
}

int min(int a, int b, int c){
    if ((a<b) && (a<c)) return a; else
        if ((b<a) && (b<c)) return b; else
            return c;
}

int main() {
    my_string s1 = {8, NULL}, s2 = {8, NULL};
    if (bufer_vvod(&s1)) return -1;
    if (bufer_vvod(&s2)) return -1;
    int n1 = s1.length, n2 = s2.length;

    int n = n2 + 1, p;
    int *M = (int *) malloc(sizeof(int)*(n1+1)*(n2+1));
    if (!M) {
        fprintf(stderr,"Don't acceess memory");
        return -1;
    }

    for (int i=0; i<=n1; ++i) M[n*i+0] = i;
    for (int i=0; i<=n2; ++i) M[n*0+i] = i;
    for (int i=1; i<=n1; ++i) {
        for (int j=1; j<=n2; ++j) {
            if (s1.s[i-1] == s2.s[j-1]) p = 0; else p = 1;
            M[n*i+j] = min(M[n*(i-1)+j] + 1, M[n*i+j-1] + 1, M[n*(i-1)+j-1] + p);
        }
    }
    printf("%d",M[n*n1+n2]);

    free(M); free(s1); free(s2);
    return 0;
}
