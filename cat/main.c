#include <stdio.h>
#include <stdlib.h>

typedef struct myy_string my_string;
struct myy_string{
    int length;
    char *s;
};
// пустые, с пробелами, a"a"a

int prov(char *s){
    if (!s) {
        fprintf(stderr,"Don't acceess memory");
        return -2;
    }
    return 0;
}

int prov2(my_string *s){
    if (!s) {
        fprintf(stderr,"Don't acceess memory");
        return -2;
    }
    return 0;
}

int bufer_vvod(my_string * s){
    s->s = (char *) malloc(sizeof(char)*(s->length)); if (prov(s->s)) return -2;
    char c; int k = -1;
    while (1) {
        c = getchar();
        //if (c==EOF) exit(0);
        if (k==s->length-1) {
            s->s = realloc(s->s, sizeof(char) * (s->length*=2)); if (prov(s->s)) return -2;
        }
        if (c=='\n') {
            s->s[++k] = '\0';
            break;
        } else
            s->s[++k] = c;
    }
    s->length = k;
    return 0;
}

int improve(my_string **arv, int *arg){
    int i;
    if (*arv) {
        (*arv) = (my_string *) realloc((*arv), sizeof(my_string) * (*arg) * 2);
        if (prov2((*arv))) return -2;
        for (i = (*arg); i < 2 * (*arg); ++i) {
            (*arv)[i].length = 8;
            (*arv)[i].s = (char *) malloc(sizeof(char) * ((*arv)[i].length + 1));
            if (prov((*arv)[i].s)) return -2;
        }
        (*arg) = (*arg) * 2;
    } else {
        (*arg) = 8;
        (*arv) = (my_string *) malloc(sizeof(my_string) * (*arg));
        if (prov2((*arv))) return -2;
        for (i=0; i<(*arg); ++i) {
            (*arv)[i].length = 8;
            (*arv)[i].s = (char *) malloc(sizeof(char)*((*arv)[i].length + 1));
            if (prov((*arv)[i].s)) return -2;
        }
    }
    return 0;
}

int improve2(my_string * s){
    s->s = realloc(s->s, sizeof(char) * ((s->length*=2) + 1));
    if (prov(s->s)) return -2;
    return 0;
}

void my_free(my_string **arv, int *arg){
    int i = 0;
    for (;i<(*arg);++i) free((*arv)[i].s);
    free((*arv));
}

int vvod(my_string **arv, int *arg) {

        my_string s1 = {8, NULL};
        if (bufer_vvod(&s1)) return -2;
        char *s =  s1.s;
        int n = s1.length, i;

        if (improve(arv, arg)) return -2;

        int q1 = 0; // мы не в двойных ковычках
        int q2 = 0; // мы не в слове
        int k = -1, j = -1, kav = 0; //j - номер слова, k - буква в слове arv[j]
        for (i=0; i<=n; ++i) {
            if (s[i]=='"') ++kav;
            if (((s[i]=='"') && (!q1)) || (s[i-1]=='"') && (s[i]=='"')) {
                if (q2){
                    (*arv)[j].length = k+1;
                    (*arv)[j].s[++k] = '\0';
                    q2 = 0;
                }
                q1 = 1;
                if ((s[i-1]=='"') && (s[i]=='"')) q1=0;
                continue;
            } // зашли в ковычки
            if ((s[i]=='"') && (q1)) {
                (*arv)[j].length = k+1;
                (*arv)[j].s[++k] = '\0';
                q1 = 0; q2 = 0; continue;
            } // вышли из ковычек
            if (q1) {
                if (!q2) {
                    q2 = 1; k = -1;
                    if (++j == (*arg)) if (improve(arv, arg)) return -2;
                }
                if (++k == (*arv)[j].length) if (improve2(&(*arv)[j])) return -2;
                (*arv)[j].s[k] = s[i];
            } else {
                if ((s[i]==' ') || (s[i]=='\t') || (s[i]=='\0')) {
                    if (q2) {
                        (*arv)[j].length = k+1;
                        (*arv)[j].s[++k] = '\0';
                        q2 = 0;
                    }
                } else {
                    if (!q2) {
                        q2 = 1; k = -1;
                        if (++j == (*arg)) if (improve(arv, arg)) return -2;
                    }
                    if (++k == (*arv)[j].length) if (improve2(&(*arv)[j])) return -2;
                    (*arv)[j].s[k] = s[i];
                }
            }
        }
        free(s);
        for (i=j+1;i<(*arg);++i) free((*arv)[i].s);
        (*arg) = j+1;

        if ((q1) || (kav % 2 == 1)) {
            fprintf(stderr,"Kavichka is out\n");
	    return -1;
        }

    return 0;
}

int main() {
    /*while (1) {
        my_string s1 = {8, NULL};
        if (bufer_vvod(&s1)) exit(1);
        printf("%s\n",s1.s);
        free(s1.s);
    }*/

    my_string *arv = NULL;
    int arg = 0, i;

    vvod(&arv,&arg);

            printf("%d\n",arg);
            for (i=0;i<arg;++i){
                printf("%d %s\n",arv[i].length, arv[i].s);
            }

    my_free(&arv,&arg);
    return 0;
}

