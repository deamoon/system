#include <pthread.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h> 
#include <semaphore.h>
#include <string.h>

int K, T, N, size, memd;
const char * mem_name = "my_memory";
const char * sem_name = "my_semaphore";
double *mut2;

void cher(const char * where) {
    if (errno) {
        perror(where);
        exit(1);
    }
}

double Random() {
    return (rand() % 10000) / 9999.0;
}

double Carlo(int i, int j) {
    memd = shm_open(mem_name, O_RDWR | O_CREAT, 0666);
    if (memd == -1) cher("shm_open");
    double * mut = NULL;
    mut = mmap(NULL, size, PROT_WRITE | PROT_READ, MAP_SHARED, memd, 0);
    if (mut == MAP_FAILED) cher("child mmap");
    close(memd);
    
    int t, R = 100*1000, res = 0;
    double x, y;
    srand((i+1)*T + j + 1 + time(NULL));
    for (t=0;t<N;++t) {
        x = Random();
        y = Random();
        if (x * x + y * y <= 1) ++res;
    }
    
    sem_t * s = sem_open(sem_name, O_CREAT, 0666, 1);
    if (s == SEM_FAILED) cher("sem_open");
    if (sem_wait(s)) cher("sem_wait");
        mut[i*T+j] = (double)(4*res) / (double)(N);
        if (munmap(mut, size)) cher("child munmap");
    sem_post(s);
    if (sem_close(s)) cher("sem_close");
}

int handler(const void *p1, const void *p2) {
    return ( *(double * const)p1 > *(double * const)p2 );
}

int main() {
    printf("%d\n", RAND_MAX);
    
    int i, j;
    double * mut = NULL;
    int *plot;
    scanf("%d %d %d",&K,&T,&N);
    size = K * T * sizeof(double);
    plot = (int *)malloc(K*T*sizeof(int)+1);
    //srand(time(NULL)); 
    
    memd = shm_open(mem_name, O_RDWR | O_CREAT, 0666);   
    if (memd == -1) cher("shm_open");
    if (ftruncate(memd, size)) cher("ftruncate");
    mut = mmap(NULL, size, PROT_WRITE | PROT_READ, MAP_SHARED, memd, 0);
    if (mut == MAP_FAILED) cher("parent mmap");
    close(memd);
    sem_t * s = sem_open(sem_name, O_CREAT, 0666, 1);
    if (s == SEM_FAILED) cher("sem_open");
        
    for (i=0;i<K;++i) {
        for (j=0;j<T;++j) {
            if (!fork()) {
                Carlo(i,j);
                exit(0);    
            }
        }
        for (j=0;j<T;++j) wait(NULL);
    }
    
    double sred = 0, disp = 0;
    

    printf("sort");
    qsort(&mut[0], K*T, sizeof(mut[0]), handler);
    //qsort(&st_path[0], path_num, sizeof(st_path[0]), cmpstringp);
    
    double min = mut[0], max = mut[K*T-1];
    double shag;
    shag = (max-min)/(32);
    double nex;
    
    for (i=0;i<K*T;++i) {
        sred += mut[i];
        plot[i]=0;
        printf("%f\n",mut[i]);
    }
    
    printf("shag = %f\n", shag);
    printf("min = %f\n", min);
    printf("max = %f\n", max);
    for (i=0;i<K*T;++i) {
        ++plot[(int) ((mut[i]-min) / shag)];
    }    
    for (i=0;i<=(int)((max-min) / shag);++i) {
        printf("%d %f\n", plot[i], min+i*shag);
    }        
        
    sred/=(K*T);
    for (i=0;i<K*T;++i) disp += (mut[i]-sred)*(mut[i]-sred);
    disp/=(K*T);    
    printf("%f %f\n", sred, disp);
    
    if (sem_unlink(sem_name)) cher("sem_unlink");
    if (munmap(mut, size)) cher("parent munmap");
    if (shm_unlink(mem_name)) cher("shm_unlink");
    return 0;
}
