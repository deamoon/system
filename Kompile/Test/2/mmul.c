#include <pthread.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h> 
#include <semaphore.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
//#include "vvod.h"
#include <sys/types.h>
#include <dirent.h>
#include <memory.h>
#include <pthread.h>
#include <sys/time.h>

const char * mem_name1 = "my_memory1";
const char * mem_name2 = "my_memory2";
const char * mem_name3 = "my_memory3";
//const char * sem_name = "my_semaphore";

void cher(const char * Where) {
    if (errno) {
        perror(Where);
        exit(1);
    }
    fprintf(stderr,"%s",Where);
    exit(1);
}

double Carlo(int n, int N, int T) {
    int i, j, *A = NULL, *B = NULL,*C = NULL, size, memd1,memd2,memd3, *D, k;    
    size = (N+1)*(N+1)*sizeof(int);
    
    memd1 = shm_open(mem_name1, O_RDWR | O_CREAT, 0666);
    memd2 = shm_open(mem_name2, O_RDWR | O_CREAT, 0666);
    memd3 = shm_open(mem_name3, O_RDWR | O_CREAT, 0666);
    if (memd1 == -1) cher("shm_open");
    if (memd2 == -1) cher("shm_open");
    if (memd3 == -1) cher("shm_open");
    //int *A = NULL,*B = NULL,*C = NULL;
    A = mmap(NULL, size, PROT_WRITE | PROT_READ, MAP_SHARED, memd1, 0);
    B = mmap(NULL, size, PROT_WRITE | PROT_READ, MAP_SHARED, memd2, 0);
    C = mmap(NULL, size, PROT_WRITE | PROT_READ, MAP_SHARED, memd3, 0);
    if (A == MAP_FAILED) cher("child mmap");
    if (B == MAP_FAILED) cher("child mmap");
    if (C == MAP_FAILED) cher("child mmap");
    close(memd1);close(memd2);close(memd3);
    
    while (!A[N*N]) {}
    
    //printf("%d\n", n);
    for(i=0;i<N;++i) {
        for(j=0;j<N;++j) {
            if (((N*i+j)%T) != n) continue; 
            for(k=0;k<N;++k) { 
                C[i*N+j] += A[i*N+k]*B[k*N+j];
            }
        }
    }      
    
    /*sem_t * s = sem_open(sem_name, O_CREAT, 0666, 1);
    if (s == SEM_FAILED) cher("sem_open");
    if (sem_wait(s)) cher("sem_wait");
        mut[i*T+j] = (double)(4*res) / (double)(N);
        if (munmap(mut, size)) cher("child munmap");
    sem_post(s);
    if (sem_close(s)) cher("sem_close");*/
    if (munmap(A, size)) cher("child munmap");
    if (munmap(B, size)) cher("child munmap");
    if (munmap(C, size)) cher("child munmap");
}

int main(int arg, char ** arv) {
    int i, N, T, j, *A = NULL, *B = NULL,*C = NULL, size, memd1,memd2,memd3, *D, k;    
    
    if (arg>3) cher("Неправильные аргументы\n");
    
    if (arg==3) {
        sscanf(arv[1], "%d", &N);
        sscanf(arv[2], "%d", &T);    
    } else { 
        cher("Не те аргументы\n");
    }  
    if ((T<=0) || (N<=0)) cher("Не те аргументы\n");
    
    size = (N+1)*(N+1)*sizeof(int);
    D = (int *)malloc(size);
    if (!D) cher("memory");
    
    memd1 = shm_open(mem_name1, O_RDWR | O_CREAT, 0666);
    memd2 = shm_open(mem_name2, O_RDWR | O_CREAT, 0666);   
    memd3 = shm_open(mem_name3, O_RDWR | O_CREAT, 0666);      
    if (memd1 == -1) cher("shm_open");
    if (memd2 == -1) cher("shm_open");
    if (memd3 == -1) cher("shm_open");
    if (ftruncate(memd1, size)) cher("ftruncate");
    if (ftruncate(memd2, size)) cher("ftruncate");
    if (ftruncate(memd3, size)) cher("ftruncate");
    A = mmap(NULL, size, PROT_WRITE | PROT_READ, MAP_SHARED, memd1, 0);
    if (A == MAP_FAILED) cher("A mmap");
    B = mmap(NULL, size, PROT_WRITE | PROT_READ, MAP_SHARED, memd2, 0);
    if (B == MAP_FAILED) cher("B mmap");
    C = mmap(NULL, size, PROT_WRITE | PROT_READ, MAP_SHARED, memd3, 0);
    if (C == MAP_FAILED) cher("C mmap");    
    close(memd1);close(memd2);close(memd3);
    //sem_t * s = sem_open(sem_name, O_CREAT, 0666, 1);
    //if (s == SEM_FAILED) cher("sem_open");    
    for(i=0;i<N;++i) {
        for(j=0;j<N;++j) {
            D[N*i+j] = 0;
            C[N*i+j] = 0;
        }
    }
    A[N*N] = 0;
        
    srand(time(NULL));
    for(i=0;i<N;++i) {
        for(j=0;j<N;++j) {
            A[N*i+j] = rand()%1000;
            B[N*i+j] = rand()%1000;
            //C[N*i+j] = rand()%1000;
            //printf("%d ",C[N*i+j]);
        }
        //printf("\n");
    }    
    /*printf("\n");
    for(i=0;i<N;++i) {
        for(j=0;j<N;++j) {
            printf("%d ",C[N*i+j]);
        }
        printf("\n");
    }    */
    
    
    for (i=0;i<T;++i) {
        if (!fork()) { //Child
            Carlo(i, N, T);
            exit(0);    
        }
    }
    
    printf("Жмите Enter\n");
    while (getchar()!='\n') {}
    
    /////////
    struct timeval t_time1, t_time2;
    struct timezone t_zone1, t_zone2;     
    if (gettimeofday(&t_time1, &t_zone1)) cher("time1");
    A[N*N] = 1;
    for (j=0;j<T;++j) wait(NULL);
    if (gettimeofday(&t_time2, &t_zone2)) cher("time2");
    
    printf("Прошло %ld с.\n", t_time2.tv_usec);
    printf("Прошло %ld с.\n", t_time1.tv_usec);
    printf("Прошло %ld мс.\n", (long int)((t_time2.tv_sec - t_time1.tv_sec)*1000 + (t_time2.tv_usec - t_time1.tv_usec)/1000));
    /////////   
   
   
    for(i=0;i<N;++i) {
        for(j=0;j<N;++j) {
            for(k=0;k<N;++k) { 
                D[i*N+j] += A[i*N+k]*B[k*N+j];
            }
        }
    }       

    int q = 1;
    for(i=0;i<N;++i) {
        for(j=0;j<N;++j) {
            if (D[N*i+j]!=C[N*i+j]) q = 0;
        }
    }
    if (q) printf("OK\n"); else printf("FAIL\n");
/* 
    for(i=0;i<N;++i) {
        for(j=0;j<N;++j) {
            printf("%d ",A[N*i+j]);
        }
        printf("\n");
    }     
    for(i=0;i<N;++i) {
        for(j=0;j<N;++j) {
            printf("%d ",B[N*i+j]);
        }
        printf("\n");
    }   
    
    for(i=0;i<N;++i) {
        for(j=0;j<N;++j) {
            printf("%d ",C[N*i+j]);
        }
        printf("\n");
    }
      
    for(i=0;i<N;++i) {
        for(j=0;j<N;++j) {
            printf("%d ",D[N*i+j]);
        }
        printf("\n");
    }     
    */
    
    //if (sem_unlink(sem_name)) cher("sem_unlink");
    free(D);
    if (munmap(A, size)) cher("A munmap");
    if (munmap(B, size)) cher("B munmap");
    if (munmap(C, size)) cher("C munmap");
    if (shm_unlink(mem_name1)) cher("shm_unlink");
    if (shm_unlink(mem_name2)) cher("shm_unlink");    
    if (shm_unlink(mem_name3)) cher("shm_unlink");        
    return 0;
}










