#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>
#include "ls_lib.h"

void ls(char * path) {
    struct stat s;
    if (stat(path, &s)) { fprintf(stderr,"%s is out",path); return; }
    if (S_ISDIR(s.st_mode)) {
        DIR * dir = opendir(path);
        if (!dir) { printf("Нет доступа"); return; }
        printf("\n%s:\n",path);
        struct dirent * ent; int q = 0;
        while ((ent = readdir(dir)) != NULL) {
            if (ent->d_name[0]=='.') continue;
            printf("%s  ", ent->d_name); q = 1;
        }
        closedir(dir);
    } else {
        printf("%s ",path);
    }    
}

void file_l(struct stat s, char * name) {
        struct passwd *p1; p1 = getpwuid(s.st_uid);
        struct group  *p2; p2 = getgrgid(s.st_gid);
  
        if (S_ISDIR(s.st_mode)) printf("d"); else printf("-");
        if (s.st_mode & S_IRUSR) printf("r"); else printf("-");
        if (s.st_mode & S_IWUSR) printf("w"); else printf("-");
        if (s.st_mode & S_IXUSR) printf("x"); else printf("-");
        if (s.st_mode & S_IRGRP) printf("r"); else printf("-");
        if (s.st_mode & S_IWGRP) printf("w"); else printf("-");
        if (s.st_mode & S_IXGRP) printf("x"); else printf("-");
        if (s.st_mode & S_IROTH) printf("r"); else printf("-");
        if (s.st_mode & S_IWOTH) printf("w"); else printf("-");
        if (s.st_mode & S_IXOTH) printf("x"); else printf("-");

        printf(" %d ", (int)s.st_nlink);
        if (p1) printf(" %s ", p1->pw_name); else printf("unknown ");
        if (p2) printf(" %s ", p2->gr_name); else printf("unknown ");
        printf("%7lld",(long long)s.st_size);
        time_t rawtime = s.st_mtime; struct tm *timeinfo;
        timeinfo = localtime(&rawtime);
        char * month[] = { "января", "февраля", "марта", "апреля", "мая",
                              "июня", "июля", "августа", "сентября", "октября",
                              "ноября","декабря"};
        printf(" %s %d %.2d:%.2d ", month[timeinfo->tm_mon], timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min);
        printf("%s\n",name);    
}

void ls_l(char * path) {
    struct stat s;
    if (stat(path, &s)) { fprintf(stderr,"%s is out",path); return; }
    if (S_ISDIR(s.st_mode)) {
        printf("%s:\n",path);
        DIR * dir = opendir(path);
        if (!dir) { printf("Нет доступа"); return; }
        struct dirent * ent;
        long long total = 0;
        while ((ent = readdir(dir)) != NULL) {
            if (ent->d_name[0]=='.') continue;

            struct stat s;
            int n1 = strlen(path), n2 = strlen(ent->d_name), i;
            char *str = malloc(sizeof(char) * (n1 + n2 + 2));
            for (i=0;i<n1;++i) str[i] = path[i];
            str[n1]='/';
            for (i=n1+1;i<n1+n2+1;++i) str[i] = ent->d_name[i-n1-1];
            str[n1+n2+1]='\0';
            stat(str, &s);
            free(str);
                
            file_l(s, ent->d_name);
            total+=s.st_blocks;
        }
        printf("итого: %lld\n",total/2);
        closedir(dir);
    } else {
        file_l(s,path);
    }
}

void ls_R(char * path, int q) {
    struct stat s;
    if (stat(path, &s)) { fprintf(stderr,"%s is out",path); return; }
    if (S_ISDIR(s.st_mode)) {
        if (q) ls_l(path); else ls(path);
        printf("\n");
        DIR *dir = opendir(path);
        if (!dir) { printf("Нет доступа"); return; }
        struct dirent * ent;
        while ((ent = readdir(dir)) != NULL) {
            if (ent->d_type == DT_DIR) {
                if (ent->d_name[0]=='.') continue;
                int n1 = strlen(path), n2 = strlen(ent->d_name), i;
                char *s = malloc(sizeof(char) * (n1 + n2 + 2));
                for (i=0;i<n1;++i) s[i] = path[i];
                s[n1]='/';
                for (i=n1+1;i<n1+n2+1;++i) s[i] = ent->d_name[i-n1-1];
                s[n1+n2+1]='\0';
                ls_R(s,q);
               free(s);
            }
        }
        closedir(dir);
    } else {
        if (q) ls_l(path); else ls(path);
    }
}
