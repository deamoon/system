#include <stdio.h>
#include <stdlib.h>
#include "ls_lib.h"

int main(int arg, char ** arv) {
    int i, q_l = 0, q_R = 0, q_other = 0;
    
    for(i=1;i<arg;++i){
        if ((arv[i][0]=='-') && (arv[i][1]=='l') && (arv[i][2]=='\0')) q_l = i; else
        if ((arv[i][0]=='-') && (arv[i][1]=='R') && (arv[i][2]=='\0')) q_R = i; else
        q_other = 1;       
    }

    if (arg == 1) {
        ls(".");

    }
    
    if (!q_other) {
        if ((q_l) && (q_R)) ls_R(".",1); else
          if (q_l) ls_l("."); else 
            if (q_R) ls_R(".",0);       

    } else {
        if ((q_l) && (q_R)) {
            for(i=1;i<arg;++i) if ((i!=q_l)&&(i!=q_R)) {ls_R(arv[i],1); printf("\n"); } 
        } else
        if (q_l) {
            for(i=1;i<arg;++i) if ((i!=q_l)&&(i!=q_R)) { ls_l(arv[i]); printf("\n"); }
        } else
        if (q_R) {
            for(i=1;i<arg;++i) if ((i!=q_l)&&(i!=q_R)) { ls_R(arv[i],0); printf("\n"); }
        } else
            for(i=1;i<arg;++i) { ls(arv[i]); printf("\n"); }
    
    }

    return 0;
}


