#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <semaphore.h>
#include <signal.h>

int memd, size, N, exx = 0;
const char * mem_name = "my_memory19";
const char * sem_name = "my_semaphore19";

void cher(const char * where) {
    if (errno) {
        perror(where);
        exit(1);
    }
}

void handler(int s) {
    exx = 1;
}

int right(int i) {
    if (i==0) return N-1; else return (i-1) % N;
}

void Clever(int pid, int i) {
    memd = shm_open(mem_name, O_RDWR | O_CREAT, 0666);
    if (memd == -1) cher("shm_open");
    int * mut = NULL;
    mut = mmap(NULL, size, PROT_WRITE | PROT_READ, MAP_SHARED, memd, 0);
    if (mut == MAP_FAILED) cher("child mmap");
    close(memd);
    
    sem_t * s = sem_open(sem_name, O_CREAT, 0666, 1);
    if (s == SEM_FAILED) cher("sem_open");
    int st = 0;
    //int st1 = -1, st2 = -1, kol = 0;
    //int min, max;
    //if (i<right(i)) { min=i; max=right(i); } else { max=i; min=right(i); }

    while (1) {     
        sem_wait(s);
          if (mut[i]==1 && mut[right(i)]==1) {
              mut[i]=0; mut[right(i)]=0;
              fprintf(stderr,"Взял %d\n",i);
              st = 1;
          }
        sem_post(s);                    
        
        if (st) {
            sleep(1); // Философ ест
            st = 0;
            sem_wait(s);
              fprintf(stderr,"Положил %d\n",i); // Философ поел
              mut[i]=1; mut[right(i)]=1;
              //sleep(2);
            sem_post(s);                
            sleep(2);
        }
    }
            
    if (sem_close(s)) cher("sem_close");    
}

//kill -SIGUSR1 getpid()

int main() {
    int i, * vilka, *pid;
    fprintf(stderr,"%d\n",getpid());
    scanf("%d",&N); 
    //N = 5;
    pid = (int *)malloc(sizeof(int)*(N+1));
    vilka = (int *)malloc(sizeof(int)*N);        
    int * mut = NULL; size = N * sizeof(int) * 2;
    pid[N] = getpid();
    
    memd = shm_open(mem_name, O_RDWR | O_CREAT, 0666);   
    if (memd == -1) cher("shm_open");
    if (ftruncate(memd, size)) cher("ftruncate");
    mut = mmap(NULL, size, PROT_WRITE | PROT_READ, MAP_SHARED, memd, 0);
    if (mut == MAP_FAILED) cher("parent mmap");
    close(memd);
    sem_t * s = sem_open(sem_name, O_CREAT, 0666, 1);
    if (s == SEM_FAILED) cher("sem_open");    
    
    signal(SIGUSR1, handler);
    for (i=0;i<N;++i) { mut[i] = 1; mut[N+i] = 0; vilka[i] = 1; }
    
    for (i=0;i<N;++i) {
        if (!(pid[i] = fork())) {
            Clever(pid[i], i);
        }
    }
    
    while (1) {
        if (exx) break;
    }
    
    for (i=0;i<N;++i) kill(pid[i], 9);
    if (sem_unlink(sem_name)) cher("sem_unlink");
    if (munmap(mut, size)) cher("parent munmap");
    if (shm_unlink(mem_name)) cher("shm_unlink");      
    fprintf(stderr,"Все завершено\n");
          
    return 0;
}











