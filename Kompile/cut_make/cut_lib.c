#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cut_lib.h"

void cut(int * number) {
    int i, q, tab = 0;
    while (1) {
        my_string s = {8, NULL};
        q = file_vvod(&s); if (!s.s) break;
        my_string *col = NULL; int col_n = 0;
        if (vvod(&col, &col_n, s, &tab)) break;
        if (!tab) { printf("%s\n",s.s); free(s.s); continue; }
        for (i=0;i<col_n;++i) if (number[i+1]) printf("%s\t",col[i].s);
        my_free(&col, &col_n);
        printf("\n");
        if (!q) break;
    }
}

int cifra(char c){
    if ((c>='0') && (c<='9')) return 1; else return 0;
}

void parser(char * str, int * arr) {
    int i, j, n = strlen(str), a = 0, b = 0, t;
    char *s = (char *) malloc(sizeof(char) * (n+3));
    s[0] = ','; for(i=0;i<n;++i) s[i+1]=str[i]; s[n+1] = ','; n = n+2;
    int t1 = 0, t2 = 0, t3 = 0;

    for(i=0;i<n;++i) {
        if (cifra(s[i])) {
            if (t3) {
                b *= 10; b += (int)s[i] - (int)'0';
            } else {
                a *= 10; a += (int)s[i] - (int)'0';
            }
        } else
        if (s[i]=='-') {
            t1=t2=t3=0;
            if (cifra(s[i-1]) && cifra(s[i+1])) t3 = 1; else
              if (cifra(s[i-1])) t1 = 1; else
                if (cifra(s[i+1])) t2 = 1;
        } else
        if (s[i]==',') {
            if (t1) b = SIZE; else
              if (t2) { t = a; a = b; b = t; a = 1; } else
                if (!t3) b = a;
            for (j=a;j<=b;++j) arr[j] = 1;
            a=b=t1=t2=t3=0;
        } else {
            fprintf(stderr,"Возможны только символы цифр, запятая, тире\n");
        }
    }
    arr[0] = 0;
}

// пустые, с пробелами, a"a"a

int prov(char *s){
    if (!s) {
        fprintf(stderr,"Don't acceess memory");
        return -2;
    }
    return 0;
}

int prov2(my_string *s){
    if (!s) {
        fprintf(stderr,"Don't acceess memory");
        return -2;
    }
    return 0;
}

int file_vvod(my_string * s) {
    if (s->s) free(s->s); s->length = 8;
    s->s = (char *) malloc(sizeof(char)*(s->length)); if (prov(s->s)) return 0;
    char c; int k = -1, q = 1;
    while (1) {
        c = getchar();
        if ((k==-1) && (c==EOF)) { free(s->s); s->s = NULL; q = 0; break; }
        if (k==s->length-1) {
            s->s = realloc(s->s, sizeof(char) * (s->length*=2)); if (prov(s->s)) return 0;
        }
        if ((c=='\n') || (c==EOF)) {
            s->s[++k] = '\0';
            if (c==EOF) q = 0;
            break;
        } else
            s->s[++k] = c;
    }
    s->length = k;
    return q;
}

int improve(my_string **arv, int *arg){
    int i;
    if (*arv) {
        (*arv) = (my_string *) realloc((*arv), sizeof(my_string) * (*arg) * 2); 
        if (prov2((*arv))) return -2;
        for (i = (*arg); i < 2 * (*arg); ++i) {
            (*arv)[i].length = 8;
            (*arv)[i].s = (char *) malloc(sizeof(char) * ((*arv)[i].length + 1)); 
            if (prov((*arv)[i].s)) return -2;
        }
        (*arg) = (*arg) * 2;
    } else {
        (*arg) = 8;
        (*arv) = (my_string *) malloc(sizeof(my_string) * (*arg));
        if (prov2((*arv))) return -2;
        for (i=0; i<(*arg); ++i) {
            (*arv)[i].length = 8;
            (*arv)[i].s = (char *) malloc(sizeof(char)*((*arv)[i].length + 1)); 
            if (prov((*arv)[i].s)) return -2;
        }
    }
    return 0;
}

int improve2(my_string * s){
    s->s = realloc(s->s, sizeof(char) * ((s->length*=2) + 1)); 
    if (prov(s->s)) return -2;
    return 0;
}

void my_free(my_string **arv, int *arg){
    int i = 0;
    for (;i<(*arg);++i) if ((*arv)[i].s) free((*arv)[i].s);
    if ((*arv)) free((*arv));  
}

int vvod(my_string **arv, int *arg, my_string str, int *tab) {
        char *s =  str.s;
        int n = str.length, i; *tab = 0;

        if (improve(arv, arg)) return -2;

        int q2 = 0; // мы не в слове
        int k = -1, j = -1, kav = 0; //j - номер слова, k - буква в слове arv[j]
        for (i=0; i<=n; ++i) {
            if (s[i]=='\t') *tab = 1;
            if ((s[i]=='\t') || (s[i]=='\0')) {
                if (q2) {
                    (*arv)[j].length = k+1;
                    (*arv)[j].s[++k] = '\0';
                    q2 = 0;
                } else {
                    if (++j == (*arg)) if (improve(arv, arg)) return -2;
                    (*arv)[j].s[0] = '\0';
                    (*arv)[j].length = 0;
                }
            } else {
                if (!q2) {
                    q2 = 1; k = -1;
                    if (++j == (*arg)) if (improve(arv, arg)) return -2;
                }
                if (++k == (*arv)[j].length) if (improve2(&(*arv)[j])) return -2; 
                (*arv)[j].s[k] = s[i];
            }
        }
        
        if ((*tab)==1) free(s);         
        for (i=j+1;i<(*arg);++i) free((*arv)[i].s);
        (*arg) = j+1;

    return 0;
}
