#include <stdio.h>
#include <stdlib.h>
#include "cut_lib.h"

int main(int arg, char ** arv) {
    int i;
    if (arg == 1) { fprintf(stderr,"%s: Введите флаги\n",arv[0]); return -1; }
    
    //парсим arg[1]
    int number[SIZE] = {0};
    parser(arv[1], number);
    
    if (arg == 2){
        cut(number);
    } else {
        for (i=2;i<arg;++i) {
            if (!freopen(arv[i],"r",stdin)) {
                fprintf(stderr,"%s: %s: Нет такого файла или каталога\n",arv[0],arv[i]);
            } else {
                cut(number);
            }
        }  
    }
    return 0;
}


