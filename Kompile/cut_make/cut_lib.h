#define SIZE 10000

void cut(int * number);
int cifra(char c);
void parser(char * str, int * arr);

typedef struct myy_string my_string;
struct myy_string{
    int length;
    char *s;
};

int prov(char *s);
int prov2(my_string *s);
int file_vvod(my_string * s);
int improve(my_string **arv, int *arg);
int improve2(my_string * s);
void my_free(my_string **arv, int *arg);
int vvod(my_string **arv, int *arg, my_string str, int *tab);
