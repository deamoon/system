#include <stdio.h>
#include <ftw.h>
#include <stdlib.h>
#include <string.h>

typedef struct myy_path my_path;
struct myy_path{
    int size;
    char s[100];  // Max размер пути файла
};

int path_num = 0;
my_path st_path[10000]; // Max кол-во файлов

int cmpstringp(const void *p1, const void *p2) {
    return ( ( ((my_path * const) p1 ) -> size ) > ( ((my_path * const) p2 ) -> size ) );
}

int fn(const char *fpath, const struct stat *sb, int typeflag) {
    if (typeflag == FTW_F) {      
        strcpy(st_path[path_num].s, fpath);
        st_path[path_num++].size = sb->st_size;
    }
    return 0;
}

void write_path(char * s, int n) {
    int k;
    for (int i = strlen(s)-1; i>=0; --i)
        if (s[i] == '/') { k = i; break; }
  
    for (int i = k+1; i < strlen(s); ++i) printf("%c",s[i]);
    printf("  -- %d\n", n);
}

int main(int argc, char *argv[]) {

    if (argc < 2) {
        fprintf(stderr,"Vvedite path\n"); 
        exit(1);  
    }

    if( ftw (argv[1], fn, 20) ) { // 20 - max кол-во доступных дескрипторов
        fprintf(stderr,"Error was\n"); 
        exit(1);  
    }
    
    qsort(&st_path[0], path_num, sizeof(st_path[0]), cmpstringp);

    for (int i=0; i<path_num; ++i) {
        //printf("%s -- %d\n", st_path[i].s, st_path[i].size);
        write_path(st_path[i].s, st_path[i].size); // Если вывод без пути
    }

    return 0;
}
