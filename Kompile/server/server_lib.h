struct Buff {
    char * data;
    int buff_length;
    int size;
    int position;
};

char * itoa(int val, int base);
void cher(const char * Where);
void make_buff(struct Buff * buff, int length);
void extend_buff(struct Buff * buff);
void free_buff(struct Buff * buff);
int get_char(int fd, struct Buff * buff);
struct Buff read_to(int fd, struct Buff * buff, char * sep);
char *URL(char *s, int fd);
char * sum(char *a, char * b);
char * sub(char *a, int b);
char * IntToStr(int n);
char *parent_dir(char *s);
void transfer_data(int fd);
