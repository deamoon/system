#include "server_lib.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <dirent.h>
#include <string.h>
#include <memory.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <pthread.h>

#define BUFF_LENGTH 16

const char * const OK = "\
HTTP/1.1 200 Ok\r\n\
Connection: close\r\n\
Accept-Ranges:  none\r\n\
Content-Type: text/html; charset=UTF-8\r\n\r\n";

const char * const NOT_FOUND = "\
HTTP/1.1 404 Not Found\r\n\
Connection: close\r\n\r\n";

const char * const BEGIN_NOT_FOUND = "<html><head><title>404 Not Found</title></head><body><h1>404 Page not found</h1><table>\
</table></body></html>\r\n";

const char * const NOT_BAD = "\
HTTP/1.1 400 Bad Request\r\n\
Connection: close\r\n\r\n\
<html><head><title>400 Bad Request</title></head><body><h1>400 Bad Request</h1><table>\
</table></body></html>\r\n";

const char * const NOT_FORBID = "\
HTTP/1.1 403 Forbidden\r\n\
Connection: close\r\n\r\n\
<html><head><title>403 Forbidden</title></head><body><h1>403 Forbidden</h1><table>\
</table></body></html>\r\n";

const char * const NOT_SUPPORT = "\
HTTP/1.1 505 HTTP Version Not Supported\r\n\
Connection: close\r\n\r\n\
<html><head><title>505 HTTP Version Not Supported</title></head><body><h1>505 HTTP Version Not Supported</h1><table>\
</table></body></html>\r\n";

const char * const BEGIN = "<html><body><head><meta charset=\"UTF-8\" /></head><h1>Deamoon http server</h1><table>";
const char * const END = "</table></body></html>\r\n";
const char * const b = "<tr><td color=\"#FF0000\">";
const char * const e = "</td></tr>";
const char * const a1_dir = "<a style=\"color:#ff0000\" href=\"";
const char * const a1_file = "<a href=\"";
const char * const a2 = "\">";
const char * const a3 = "</a>";
const char * const parent = "..";

char * itoa(int val, int base) {
    if (val == 0) {
        return "0";
    }
    static char buf[32];
    memset(buf, 0, 32);

    int i = 30;
    for(; val && i; --i, val /= base)
        buf[i] = "0123456789abcdef"[val % base];
    return &buf[i + 1];
}

void cher(const char * Where) {
    if (errno) {
        //fprintf(stderr,"%f", Where);
        perror(Where);
        exit(1);
    }
}

void make_buff(struct Buff * buff, int length) {
    buff->data = (char *)malloc(sizeof(char) * length);
    buff->buff_length = length;
    buff->size = 0;
    buff->position = 0;
}

void extend_buff(struct Buff * buff) {
    char * new_data = (char *)malloc(sizeof(char) * buff->buff_length * 2);
    int i;
    for (i = 0; i < buff->size; ++i) {
        new_data[i] = buff->data[i];
    }
    free(buff->data);
    buff->data = new_data;
    buff->buff_length *= 2;
}

void free_buff(struct Buff * buff) {
    free(buff->data);
    buff->data = NULL;
    buff->buff_length = 0;
    buff->size = 0;
    buff->position = 0;
}

int get_char(int fd, struct Buff * buff) {
    if (buff->position == buff->size) {
        buff->position = 0;
        buff->size = 0;
        int r = read(fd, buff->data, buff->buff_length);
        if (r > 0) {
            buff->size = r;
        }
    }
    if (buff->position == buff->size) {
        return EOF;
    }
    return buff->data[buff->position++];
}

struct Buff read_to(int fd, struct Buff * buff, char * sep) {
    const int sep_length = strlen(sep);
    struct Buff res;
    make_buff(&res, BUFF_LENGTH);
    while (1) {
        if (res.size == res.buff_length) {
            extend_buff(&res);
        }
        int c = get_char(fd, buff);
        if (c == EOF) {
            break;
        }
        res.data[res.size++] = (char)c;
        if (res.size < sep_length) {
            continue;
        }
        if (!strncmp(sep, &res.data[res.size - sep_length], sep_length)) {
            res.size -= sep_length;
            break;
        }
    }
    return res;
}


///////////////////////////////////////////////////////////////////////////////////

char *URL(char *s, int fd) {
    int i, a = 0, b = 0, q = 0;
    char *res;
    if (!s) {
        res = NULL;
        //fprintf(stderr,"Я закрылся наверное");
        return res;
    }
    for (i=0;i<strlen(s)-7;++i){
        if ((s[i]=='H')&&(s[i+1]=='T')&&(s[i+2]=='T')&&(s[i+3]=='P')&&
            (s[i+4]=='/')&&(s[i+5]=='1')&&(s[i+6]=='.')&&(s[i+7]=='1')
           ) { q = 1; break; }
    }
    if (!q) {
        send(fd, NOT_SUPPORT, strlen(NOT_SUPPORT), 0);
        res = NULL;
        return res;
    }
    
    for (i=0;i<strlen(s);++i){
        if ((s[i]=='/') && (!a)) a = i;
        if ((s[i]==' ') && (a)) {
            b = i; 
            break;
        }    
    }
    ++a; // выкинул слэш
    res = (char *)malloc((b-a+2)*sizeof(char));
    for (i=a;i<b;++i){
        res[i-a] = s[i];
    }
    if ((res[b-a-1] == '/') && (b>a)) res[b-a] = '\0'; else {
        res[b-a] = '/'; res[b-a+1] = '\0';
    }    
    
    if ((a==0) || (b==0)) {
        send(fd, NOT_BAD, strlen(NOT_BAD), 0); 
        free(res);
        res = NULL;
    }    
        
    return res;
}

char * sum(char *a, char * b) {
    int i, a_length = strlen(a), b_length = strlen(b);
    char *res = (char *)malloc((a_length+b_length+1)*sizeof(char));
    for (i=0;i<a_length;++i) res[i] = a[i];
    for (i=0;i<b_length;++i) res[i + a_length] = b[i];    
    res[a_length + b_length]='\0';
    return res;
}
char * sub(char *a, int b) {
    int i, a_length = strlen(a);
    char *res = (char *)malloc((a_length+1-b)*sizeof(char));
    for (i=0;i<a_length-b;++i) res[i] = a[i];
    res[a_length - b]='\0';
    return res;
}

char * IntToStr(int n) {
    int k = 0, i, t; char s[20];
    while (n!=0) {
        s[k] = (char)(n%10 + (int)('0'));
        ++k;
        n = n/10;
    }
    char *res = (char *)malloc((k+1)*sizeof(char));
    t = k;
    for(i=0;i<k;++i) {
        res[i] = s[--t];    
    }
    res[k] = '\0';
    return res;
}


char *parent_dir(char *s) {
    int i, q;
    for (i=strlen(s)-2;i>=0;--i) {
        if (s[i]=='/') {
            q = i; break;
        }
    }
    char *res;
    if (q==0) {
        res = (char *)malloc((2)*sizeof(char));
        res[0] = '/'; res[1] = '\0';
    } else {
        res = (char *)malloc((q+1)*sizeof(char));
        for (i=0;i<q;++i) {
            res[i] = s[i];
        }
        res[q] = '\0';
    }    
    return res;
}

void transfer_data(int fd) {
    int i;

    struct Buff buff;
    make_buff(&buff, BUFF_LENGTH);
    struct Buff lines[16];
    int line_count = 0;
    while (1) {
        struct Buff line = read_to(fd, &buff, "\r\n");
        if (line.size == 0) {
            free_buff(&line);
            break;
        }
        if (line_count >= 16) {
            free_buff(&line);
            continue;
        }
        lines[line_count++] = line;
    }
    
    char *url = URL(lines[0].data, fd), *path;
    if (!url) {
        for (i = 0; i < line_count; ++i) {
            free_buff(&lines[i]);
        }
        free_buff(&buff);
        return;
    }    
    
    if ((url[0]=='/') && (url[1]=='\0')) {path = (char *)malloc(3*sizeof(char)); path[0]='.'; path[1]='/'; path[2]='\0';}
      else path = sum("",url);
    char *path2 = sub(path, 1);
        
    struct stat url_stat;
    if (stat(path2, &url_stat)) { // Кидаем ошибку нет файла 
        //fprintf(stderr,"%s is out\n",path); 
        send(fd, NOT_FOUND, strlen(NOT_FOUND), 0);
        send(fd, BEGIN_NOT_FOUND, strlen(BEGIN_NOT_FOUND), 0);                
    } else
    if (S_ISDIR(url_stat.st_mode)) { // Если папка
        DIR *dir = opendir(path);
        if (!dir) { // Ошибка нет прав
            send(fd, NOT_FORBID, strlen(NOT_FORBID), 0);
            //fprintf(stderr,"Нет прав на папку");
        } else { // ls -l  
            send(fd, OK, strlen(OK), 0);
            send(fd, BEGIN, strlen(BEGIN), 0); 
            struct dirent * ent;
            int index = 0;
    
            if (strcmp(url,"/")) { // вывод ..
                send(fd, b, strlen(b), 0); // <tr><td color=\"#FF0000\">          
                send(fd, a1_dir, strlen(a1_dir), 0); // <a style="color:#ff0000" href="
                char *temp2 = sum("/", path);
                char *temp3 = parent_dir(temp2);
                   send(fd, temp3, strlen(temp3), 0); // Ссылка
               free(temp2); free(temp3);
                send(fd, a2, strlen(a2), 0); // ">
                send(fd, "..", 2, 0);
                send(fd, a3, strlen(a3), 0); // </a>
                 send(fd, e, strlen(e), 0); // </td></tr>       
            }
            
            ////////////////////////////////////////////////////////
            
            while ((ent = readdir(dir)) != NULL) {
               if (!strcmp(ent->d_name,".")) continue;
               if (!strcmp(ent->d_name, "..")) continue;
               char * n = ent->d_name;
               char *path_n = sum(path, n); 
               struct stat s; stat(path_n, &s);             
               if (S_ISDIR(s.st_mode)) {
                send(fd, "<tr>", 4, 0);
                  send(fd, "<td>", 4, 0);
                  if (S_ISDIR(s.st_mode)) send(fd, a1_dir, strlen(a1_dir), 0); else 
                    send(fd, a1_file, strlen(a1_file), 0);
                    char *temp;
                    if (!strcmp(url,"/")) temp = sum("/",n); 
                      else temp = sum("/",path_n);
                      send(fd, temp, strlen(temp), 0); // Ссылка
                    free(temp); 
                  free(path_n);
                
                  send(fd, a2, strlen(a2), 0);
                  send(fd, n, strlen(n), 0);
                  send(fd, a3, strlen(a3), 0);// </a>
                send(fd, "</td>", 5, 0);
                
                send(fd, "<td>", 4, 0);
                  char s_size[20];
                  sprintf(s_size, "%7lld",(long long)s.st_size);
                  send(fd, s_size, strlen(s_size), 0);
                send(fd, "</td>", 5, 0);  
                send(fd, "<td>", 4, 0);
                  time_t rawtime = s.st_mtime; struct tm *timeinfo;
                  timeinfo = localtime(&rawtime);
                  char * month[] = { "января", "февраля", "марта", "апреля", "мая",
                              "июня", "июля", "августа", "сентября", "октября",
                              "ноября","декабря"};
                  char s_time[80];
                  sprintf(s_time, " %d %s %.2d:%.2d ", timeinfo->tm_mday, month[timeinfo->tm_mon], timeinfo->tm_hour, timeinfo->tm_min);
                  send(fd, s_time, strlen(s_time), 0);
                send(fd, "</td>", 5, 0);
                
                send(fd, "</tr>", 5, 0);
               }    
            }
            closedir(dir); DIR *dir = opendir(path);
            while ((ent = readdir(dir)) != NULL) {
               if (!strcmp(ent->d_name,".")) continue;
               if (!strcmp(ent->d_name, "..")) continue;
               char * n = ent->d_name;
               char *path_n = sum(path, n); 
               struct stat s; stat(path_n, &s);          
               if (!S_ISDIR(s.st_mode)) {
                send(fd, "<tr>", 4, 0);                
                  send(fd, "<td>", 4, 0);
                  if (S_ISDIR(s.st_mode)) send(fd, a1_dir, strlen(a1_dir), 0); else 
                    send(fd, a1_file, strlen(a1_file), 0);
                    char *temp;
                    if (!strcmp(url,"/")) temp = sum("/",n); 
                      else temp = sum("/",path_n);
                      send(fd, temp, strlen(temp), 0); // Ссылка
                    free(temp); 
                  free(path_n);
                
                  send(fd, a2, strlen(a2), 0);
                  send(fd, n, strlen(n), 0);
                  send(fd, a3, strlen(a3), 0);// </a>
                send(fd, "</td>", 5, 0);
                
                send(fd, "<td>", 4, 0);
                  char s_size[20];
                  sprintf(s_size, "%7lld",(long long)s.st_size);
                  send(fd, s_size, strlen(s_size), 0);
                send(fd, "</td>", 5, 0);  
                send(fd, "<td>", 4, 0);
                  time_t rawtime = s.st_mtime; struct tm *timeinfo;
                  timeinfo = localtime(&rawtime);
                  char * month[] = { "января", "февраля", "марта", "апреля", "мая",
                              "июня", "июля", "августа", "сентября", "октября",
                              "ноября","декабря"};
                  char s_time[80];
                  sprintf(s_time, " %d %s %.2d:%.2d ", timeinfo->tm_mday, month[timeinfo->tm_mon], timeinfo->tm_hour, timeinfo->tm_min);
                  send(fd, s_time, strlen(s_time), 0);
                send(fd, "</td>", 5, 0);
                
                send(fd, "</tr>", 5, 0);    
               }
            }            
            
            //////////////////////////////////////////////////////
            
            send(fd, END, strlen(END), 0);        
        }
        closedir(dir);
    } else { // Это файл
        FILE *in = fopen(path2, "rb");
        if (!in) { // Опять нет прав
            send(fd, NOT_FORBID, strlen(NOT_FORBID), 0);
            //fprintf(stderr,"Нет прав на файл");
        } else { // Отдаем на скачку
            struct stat s; stat(path2, &s);
            char header[200];
		sprintf(header, 
		"HTTP/1.1 200 OK\r\n"
		"Content-Type: application/octetstream\r\n"
		"Content-Length: %i\r\n"
		"Accept-Ranges: bytes\r\n"
		"Connection: close\n"
			"\r\n", (int)s.st_size);            
            
            send(fd, header, strlen(header), 0);
            char buf[1024];
            int rd;
            while ((rd = fread(buf,1,1024,in)) > 0) {
                send(fd,buf,rd,0);
            } 
        }
    }
    for (i = 0; i < line_count; ++i) {
        free_buff(&lines[i]);
    }
    free_buff(&buff); free(url); free(path); free(path2); 
}


////////////////////////////////////////////////////////////////////////////////////

