#include "server_lib.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <dirent.h>
#include <string.h>
#include <memory.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <pthread.h>

#define NUM_THREADS 100

// Сервер должен быть многопоточным – 
// поддерживать подключение нескольких клиентов одновременно

int my_thread[NUM_THREADS];
char my_sin_addr[NUM_THREADS][200];
int my_sin_port[NUM_THREADS];
pthread_mutex_t m;

struct thread_info {
    int soc;
    int id;
};

int Free_Thread() {
    int i, t;
    for(i=0;i<NUM_THREADS;++i) {
        //pthread_mutex_lock(&m);
          t = my_thread[i];
        //pthread_mutex_unlock(&m);
        if (t==0) return i;   
    }
    fprintf(stderr,"Слишком много опдключений\n");
    pthread_mutex_destroy(&m);
    fprintf(stderr, "Сервер упал\n");
    cher("listen");    
}

void * PrintHello(void * t) {
    int id = (int) t;
    pthread_mutex_lock(&m);
      int rs = my_thread[id];
    pthread_mutex_unlock(&m);  
    transfer_data(rs);
    close(rs);
    pthread_mutex_lock(&m);
      //fprintf(stderr, "Closed: %s:%d\n", inet_ntoa(remote_addr.sin_addr), (int)ntohs(remote_addr.sin_port));
      fprintf(stderr, "Closed: %s:%d\n", my_sin_addr[id], my_sin_port[id]);
      my_thread[id] = 0;
    pthread_mutex_unlock(&m);  
    pthread_exit(NULL);
}

int main() {
    int i, s, t;
    s = socket(AF_INET, SOCK_STREAM, 0);
    if (s == -1) {
        cher("socket");
    }
    struct sockaddr_in local_addr;
    local_addr.sin_family = AF_INET;
    local_addr.sin_port = htons(8080);
    if (!local_addr.sin_port){
        cher("htons");
    }
    local_addr.sin_addr.s_addr = inet_addr("0.0.0.0");
    if (!bind(s, (const struct sockaddr *)&local_addr, sizeof(struct sockaddr_in))) {
        cher("bind");
    }
    
    pthread_t threads[NUM_THREADS]; pthread_mutex_init(&m, NULL);
    for(i=0;i<NUM_THREADS;++i) my_thread[i] = 0; // Свободны
    while (!listen(s, 10)) {
        struct sockaddr_in remote_addr;
        socklen_t addr_len = sizeof(struct sockaddr_in);
        int rs = accept(s, (struct sockaddr *)&remote_addr, &addr_len);
        if (rs == -1) {
            cher("accept");
        }
        
        pthread_mutex_lock(&m);
          t = Free_Thread(); my_thread[t] = rs;
          sprintf(my_sin_addr[t],"%s",inet_ntoa(remote_addr.sin_addr));
          my_sin_port[t] = (int)ntohs(remote_addr.sin_port);
          fprintf(stderr, "Connected: %s:%d\n", my_sin_addr[t], my_sin_port[t]);
        pthread_mutex_unlock(&m);
        
        pthread_create(&threads[t], NULL, PrintHello, (void *)t);    
    }
    
    pthread_mutex_destroy(&m);
    fprintf(stderr, "Сервер упал\n");
    cher("listen");
    return 0;
}



