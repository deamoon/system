#include <stdio.h>
#include <stdlib.h>

typedef struct myy_string my_string;

struct myy_string{
    int length;
    char *s;
};

void reverse(my_string * s){
    char t;
    for (int i=0; i < s->length/2; ++i){
        t = s->s[i];
        s->s[i] = s->s[s->length-i-1];
        s->s[s->length-i-1] = t;
    }
}

int max(int a, int b){
    if (a>b) return a; else return b;
}

int ord(char c){
    return (int) c - (int) '0';
}
char chr(int n){
    return (char) (n + (int) '0');
}

int sum(my_string * s1, my_string * s2, my_string * s3) {
    free(s3->s);
    s3->s = (char *) malloc(sizeof(char)*(max(s1->length, s2->length) + 1));
    if (!s3->s) {
        fprintf(stderr,"Don't acceess memory");
        return -1;
    }
    s3->length = max(s1->length, s2->length);

    reverse(s1); reverse(s2);
    if (s1->length > s2->length)
        for (int i=s2->length; i < s1->length; ++i) s2->s[i] = '0';
    else
        for (int i=s1->length; i < s2->length; ++i) s1->s[i] = '0';

    int p = 0;
    for (int i=0; i<s3->length; ++i) {
        s3->s[i] = chr((ord(s1->s[i]) + ord(s2->s[i]) + p) % 10);
        p = (ord(s1->s[i]) + ord(s2->s[i]) + p) / 10;
    }
    if (p) {
        s3->s[s3->length] = chr(p);
        ++s3->length;
    }
    reverse(s3); reverse(s2); reverse(s1);
    return 0;
}

int prisv(my_string * s1, my_string * s2){
    free(s1->s);
    s1->s = (char *) malloc(sizeof(char)*s2->length);
    if (!s1->s) {
        fprintf(stderr,"Don't acceess memory");
        return -1;
    }
    s1->length = s2->length;
    for (int i=0; i<s1->length; ++i){
        s1->s[i] = s2->s[i];
    }
    return 0;
}

int main()
{
    my_string s1 = {8, NULL}, s2 = {8, NULL}, s3 = {8,NULL};

    s1.length = 1; s2.length = 1;
    s1.s = (char *) malloc(sizeof(char)*s1.length); s2.s = (char *) malloc(sizeof(char)*s2.length);
    if ((!s1) || (!s2)) {
        fprintf(stderr,"Don't acceess memory");
        return -1;
    }
    s1.s[0] = '1'; s2.s[0] = '1';

    int n;
    scanf("%d",&n);
    if (n<=2) printf("1"); else {
        for (int i=0;i<(n-1)/2; ++i){
            sum(&s1,&s2,&s3);
            if (prisv(&s1,&s3)) return -1;
            sum(&s1,&s2,&s3);
            if (prisv(&s2,&s3)) return -1;
        }
        if (n%2!=0) printf("%s\n",s1.s); else printf("%s\n",s2.s);
    }

    free(s1); free(s2); free(s3);
    return 0;
}
