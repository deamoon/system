#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

main() {
	int s, c, sz;
	struct sockaddr_in ssa, csa;
	struct sockaddr *sp, *cp;
	struct hostent *rhost;
	char *host, *tstr;
	time_t itime;

	sp=(struct sockaddr *)&ssa;
	cp=(struct sockaddr *)&csa;
	sz=sizeof(ssa);

	// Создаём сокет
	s=socket(AF_INET, SOCK_STREAM, 0);
	if(s == -1){
		perror("Невозможно создать сокет");
		exit(1);
	}

	// Резервируем порт 13
	ssa.sin_family = AF_INET;
	ssa.sin_port = htons(13);
	ssa.sin_addr.s_addr = INADDR_ANY;

	if(bind(s, sp, sz) == -1){
		perror("Невозможно занять порт");
		exit(1);
	}

	// Переводим сокет в режим ожидания соединения
	if(listen(s, 0) == -1){
		perror("Невозможно перейти в режим ожидания");
		exit(1);
	}

	while(1){
		// Принимаем соединение
		if((c = accept(s, cp, &sz)) == -1) {
			perror("Ошибка при выполнении accept");
			exit(1);
		}
		
		// Преобразуем адрес хоста отправителя в его имя
		rhost=gethostbyaddr((char*)(&csa.sin_addr), 
				sizeof(csa.sin_addr), AF_INET);
		if(h_errno){
			printf("gethostbyaddr error: %d\n", h_errno);
			host=inet_ntoa(csa.sin_addr);
		} else {
			host=rhost->h_name;
		}

		// Получаем строку, содержащую дату и время
		if((itime = time(NULL)) < 0){
			perror("Не удалось получить время");
			exit(1);
		}
		tstr = ctime(&itime);

		// Выводим время поступления запроса,
		// адрес и порт отправителя
		printf("%s request from %s:%d\n", tstr, host,
				htons(csa.sin_port));

		// Отправляем дату и время клиенту
		send(c, tstr, strlen(tstr), 0);

		// Закрываем соединение
		close(c);
	}
}
