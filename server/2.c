#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define BUFSZ 128

main(int argc, char *argv[]) {
	int s, sz, i;
	struct sockaddr_in ssa;
	struct sockaddr *sp;
	struct in_addr sip;
	char buf[BUFSZ];

	sp=(struct sockaddr *)&ssa;
	sz=sizeof(ssa);

	if(argc!=2){
		// Помощь по использованию команды
		printf("Использование: %s ip-адрес\n",argv[0]);
		exit(1);
	}
	if(inet_aton(argv[1], &sip) != 1){
		printf("Неправильно задан адрес сервера\n");
		exit(1);
	}
	// Создаём сокет
	s=socket(AF_INET, SOCK_STREAM, 0);
	if(s == -1){
		perror("Невозможно создать сокет");
		exit(1);
	}

	// Задаём адрес сервера
	ssa.sin_family = AF_INET;
	ssa.sin_port = htons(13);
	ssa.sin_addr = sip;

	// Устанавливаем соединение
	if(connect(s, sp, sz) == -1){
		perror("Не удалось установить соединение");
		exit(1);
	}

	// Получаем данные от сервера
	while((i=recv(s, buf, BUFSZ, 0)) > 0)
		write(1, buf, i);

}

