#include "server_lib.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <dirent.h>
#include <string.h>
#include <memory.h>
#include <sys/stat.h>
#include <fcntl.h>

const char * const OK = "\
HTTP/1.1 200 Ok\r\n\
Connection: close\r\n\
Accept-Ranges:  none\r\n\
Content-Type: text/html; charset=UTF-8\r\n\r\n";

const char * const NOT_FOUND = "\
HTTP/1.1 404 Not Found\r\n\
Connection: close\r\n\r\n";

const char * const BEGIN_NOT_FOUND = "<html><body><h1>404 Page not found</h1><table>\
</table></body></html>\r\n";

const char * const BEGIN = "<html><body><h1>Simple http server</h1><table>";
const char * const END = "</table></body></html>\r\n";
const char * const b = "<tr><td color=\"#FF0000\">";
const char * const e = "</td></tr>";
const char * const a1_dir = "<a style=\"color:#ff0000\" href=\"";
const char * const a1_file = "<a href=\"";
const char * const a2 = "\">";
const char * const a3 = "</a>";
const char * const parent = "..";

char *URL(char *s) {
    int i, a = 0, b = 0;
    for (i=0;i<strlen(s);++i){
        if ((s[i]=='/') && (!a)) a = i;
        if ((s[i]==' ') && (a)) {
            b = i; 
            break;
        }    
    }
    ++a; // выкинул слэш
    char *res = (char *)malloc((b-a+2)*sizeof(char));
    for (i=a;i<b;++i){
        res[i-a] = s[i];
    }
    if ((res[b-a-1] == '/') && (b>a)) res[b-a] = '\0'; else {
        res[b-a] = '/'; res[b-a+1] = '\0';
    }    
    return res;
}

char * sum(char *a, char * b) {
    int i, a_length = strlen(a), b_length = strlen(b);
    char *res = (char *)malloc((a_length+b_length+1)*sizeof(char));
    for (i=0;i<a_length;++i) res[i] = a[i];
    for (i=0;i<b_length;++i) res[i + a_length] = b[i];    
    res[a_length + b_length]='\0';
    return res;
}
char * sub(char *a, int b) {
    int i, a_length = strlen(a);
    char *res = (char *)malloc((a_length+1-b)*sizeof(char));
    for (i=0;i<a_length-b;++i) res[i] = a[i];
    res[a_length - b]='\0';
    return res;
}
void transfer_data(int fd) {
    int i;

    struct Buff buff;
    make_buff(&buff, BUFF_LENGTH);
    struct Buff lines[16];
    int line_count = 0;
    while (1) {
        struct Buff line = read_to(fd, &buff, "\r\n");
        if (line.size == 0) {
            free_buff(&line);
            break;
        }
        if (line_count >= 16) {
            free_buff(&line);
            continue;
        }
        lines[line_count++] = line;
    }
    
    char *url = URL(lines[0].data), *path;
    if ((url[0]=='/') && (url[1]=='\0')) {path = (char *)malloc(3*sizeof(char)); path[0]='.'; path[1]='/'; path[2]='\0';}
      else 
    path = sum("",url);
    
    char *path2 = sub(path, 1);
    
    struct stat url_stat;
    if (stat(path2, &url_stat)) { 
        fprintf(stderr,"%s is out",path); 
        send(fd, NOT_FOUND, strlen(NOT_FOUND), 0);
        send(fd, BEGIN_NOT_FOUND, strlen(BEGIN_NOT_FOUND), 0);        
        // Кидаем ошибку нет файла 
    } else
    if (S_ISDIR(url_stat.st_mode)) {
        send(fd, OK, strlen(OK), 0);
        send(fd, BEGIN, strlen(BEGIN), 0);  
        DIR *dir = opendir(path);
        struct dirent * ent;
        int index = 0;
        
        send(fd, url, strlen(url), 0);
        send(fd, "</br>", 5, 0);
        send(fd, path, strlen(path), 0);
        send(fd, "</br>", 5, 0);
        send(fd, path2, strlen(path2), 0);
        send(fd, "</br>", 5, 0);        
        
        while ((ent = readdir(dir)) != NULL) {
            if (!strcmp(ent->d_name,".")) continue;
            if (!strcmp(ent->d_name, "..")) {
                //обработка parent
                continue;
            }
            send(fd, b, strlen(b), 0);            
            char * n = ent->d_name;
            
            char *path_n = sum(path, n); 
              send(fd, path_n, strlen(path_n), 0);
              send(fd, "</br>", 5, 0);              
              struct stat s; stat(path_n, &s);
              if (S_ISDIR(s.st_mode)) send(fd, a1_dir, strlen(a1_dir), 0); else 
                send(fd, a1_file, strlen(a1_file), 0);
                char *temp;
                if (!strcmp(url,"/")) temp = sum("/",n); 
                  else temp = sum("/",path_n);
                  send(fd, temp, strlen(temp), 0); // Ссылка
                free(temp);
            free(path_n);
            
            send(fd, a2, strlen(a2), 0);
            send(fd, n, strlen(n), 0);
            send(fd, a3, strlen(a3), 0);
                    
            send(fd, e, strlen(e), 0);
        }
        closedir(dir);
        
        send(fd, END, strlen(END), 0);        
    } else {
        char buf[1024];
        int rd;
        FILE *in = fopen(path2, "rb");
        while ((rd = fread(buf,1,1024,in)) > 0) {
            send(fd,buf,rd,0);
        }    
    }
    for (i = 0; i < line_count; ++i) {
        free_buff(&lines[i]);
    }
    free_buff(&buff); free(url); free(path); free(path2); 
}

int main() {
    int s = socket(AF_INET, SOCK_STREAM, 0), i;
    if (s == -1) {
        cher("socket");
    }
    struct sockaddr_in local_addr;
    local_addr.sin_family = AF_INET;
    local_addr.sin_port = htons(8080);
    local_addr.sin_addr.s_addr = inet_addr("0.0.0.0");
    if (!bind(s, (const struct sockaddr *)&local_addr, sizeof(struct sockaddr_in))) {
        cher("bind");
    }
    while (!listen(s, 10)) {
        struct sockaddr_in remote_addr;
        socklen_t addr_len = sizeof(struct sockaddr_in);
        int rs = accept(s, (struct sockaddr *)&remote_addr, &addr_len);
        
        //recv(rs, bufer, 1024, int flags)
        //char bufer[1024];
	//while((i=recv(rs, bufer, 1024, 0)) > 0)
	//	write(1, bufer, i);
		        
        if (rs == -1) {
            cher("accept");
        }
        fprintf(stderr, "Connected: %s:%d\n", inet_ntoa(remote_addr.sin_addr), (int)ntohs(remote_addr.sin_port));
        transfer_data(rs);
        close(rs);
        fprintf(stderr, "Closed: %s:%d\n", inet_ntoa(remote_addr.sin_addr), (int)ntohs(remote_addr.sin_port));
    }
    cher("listen");
    return 0;
}




