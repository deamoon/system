#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <memory.h>

bool Work() {
    int s = socket(AF_INET, SOCK_STREAM, 0);
    if (s < 0)
        return false;
    hostent *serv = gethostbyname("acm.timus.ru");
    if (serv == NULL)
        return false;
    sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    memcpy(&addr.sin_addr.s_addr, serv->h_addr, serv->h_length);
    addr.sin_port = htons(80);
    if (connect(s, reinterpret_cast<sockaddr*>(&addr), sizeof(addr)) < 0)
        return false;
    const char query[] = "GET / HTTP/1.1\r\n"
"Host: acm.timus.ru\r\n"
"User-Agent: fivt robot\r\n"
"Accept: text/html\r\n"
"Connection: close\r\n"
"Cookie: Language=10\r\n\r\n";
    if (write(s, query, strlen(query)) < 0)
        return false;
    for (; ;) {
        char buf[1000];
        int readed = read(s, buf, sizeof(buf));
        if (readed < 0)
            return false;
        if (readed == 0)
            break;
        for (int i = 0; i < readed; ++i)
            std::cout << buf[i];
    }
    close(s);
    return true;
}

int main() {
    Work();
    return 0;
}
