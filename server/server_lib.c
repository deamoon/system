#include "server_lib.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <dirent.h>
#include <string.h>
#include <memory.h>
#include <sys/stat.h>
#include <fcntl.h>

char * itoa(int val, int base) {
    if (val == 0) {
        return "0";
    }
    static char buf[32];
    memset(buf, 0, 32);

    int i = 30;
    for(; val && i; --i, val /= base)
        buf[i] = "0123456789abcdef"[val % base];
    return &buf[i + 1];
}

void cher(const char * Where) {
    if (errno) {
        perror(Where);
        exit(1);
    }
}

void make_buff(struct Buff * buff, int length) {
    buff->data = (char *)malloc(sizeof(char) * length);
    buff->buff_length = length;
    buff->size = 0;
    buff->position = 0;
}

void extend_buff(struct Buff * buff) {
    char * new_data = (char *)malloc(sizeof(char) * buff->buff_length * 2);
    int i;
    for (i = 0; i < buff->size; ++i) {
        new_data[i] = buff->data[i];
    }
    free(buff->data);
    buff->data = new_data;
    buff->buff_length *= 2;
}

void free_buff(struct Buff * buff) {
    free(buff->data);
    buff->data = NULL;
    buff->buff_length = 0;
    buff->size = 0;
    buff->position = 0;
}

int get_char(int fd, struct Buff * buff) {
    if (buff->position == buff->size) {
        buff->position = 0;
        buff->size = 0;
        int r = read(fd, buff->data, buff->buff_length);
        if (r > 0) {
            buff->size = r;
        }
    }
    if (buff->position == buff->size) {
        return EOF;
    }
    return buff->data[buff->position++];
}

struct Buff read_to(int fd, struct Buff * buff, char * sep) {
    const int sep_length = strlen(sep);
    struct Buff res;
    make_buff(&res, BUFF_LENGTH);
    while (1) {
        if (res.size == res.buff_length) {
            extend_buff(&res);
        }
        int c = get_char(fd, buff);
        if (c == EOF) {
            break;
        }
        res.data[res.size++] = (char)c;
        if (res.size < sep_length) {
            continue;
        }
        if (!strncmp(sep, &res.data[res.size - sep_length], sep_length)) {
            res.size -= sep_length;
            break;
        }
    }
    return res;
}
