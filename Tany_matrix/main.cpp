#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
using namespace std;
// Функция решения системы линейных алгебраических уравненй методом Гаусса
int SLAU(double **matrica_a, int n, double *massiv_b, double *x) {
  int i,j,k,r;
  double c,M,max,s;
  double **a, *b;
  a = new double *[n];
  for(i=0;i<n;i++)
    a[i]=new double[n];
  b=new double [n];
  for(i=0;i<n;i++)
    for(j=0;j<n;j++)
      a[i][j] = matrica_a[i][j];
  for(i=0;i<n;i++)
    b[i] = massiv_b[i];
    for(k=0;k<n;k++) {
      max = fabs(a[k][k]);
      r=k;
      for(i=k+1;i<n;i++)
        if (fabs(a[i][k])>max) {
          max=fabs(a[i][k]);
          r=i;
        }
      for(j=0;j<n;j++) {
        c=a[k][j];
        a[k][j] = a[r][j];
        a[r][j] = c;
      }
      c=b[k];
      b[k]=b[r];
      b[r]=c;
      for(i=k+1;i<n;i++) {
        for(M=a[i][k]/a[k][k],j=k;j<n;j++) a[i][j]-=M*a[k][j];
        b[i]-=M*b[k];
      }
    }
    if (a[n-1][n-1]==0)
      if (b[n-1]==0) return-1; else return -2;
    else {
      for(i=n-1;i>=0;i--) {
        for (s=0,j=i+1;j<n;j++) s+=a[i][j]*x[j];
        x[i]=(b[i]-s)/a[i][i];
      }
      return 0;
    }
  for(i=0;i<n;i++)
  delete [] a[i];
  delete [] a;
  delete [] b;
}
//Функция вычисления обратной матрицы
int INVERSE(double **a, int n, double **y) {
//Формальные параметры функции:
//a - исходная матрица, n - размерность матрицы, y - обратная матрица.
//Функция будет возвращать 0, если обратная матрица существует, -1 - в противном случае
  int i,j,res;
  double *b, *x;
  //Выделение памяти для промежуточных массивов b и x
  b = new double [n];
  x = new double [n];
  for(i=0;i<n;i++) {
  //Формирование вектора правых частей для нахожденя i-го столбца матрицы
    for(j=0;j<n;j++)
      if (j==i) b[j]=1; else b[j]=0;
  //Нахождение i-го столбца матрицы путем решения СЛАУ Ax=b методом Гаусса
      res = SLAU(a,n,b,x);
  //Если решение СЛАУ не найдено, то невозможно вычислить обратную матрицу
      if (res!=0) break;
        else
  //Формирование i-го столбца обратной матрицы
          for(j=0;j<n;j++) y[j][i]=x[j];
  }
  // Освобождение памяти
  delete [] x;
  delete [] b;
  //Проверка существования обратной матрицы,еслирешение одного из
  //уравнений вида Ax=b не существует, то невозможно найти обратную
  //матрицу, и функция INVERSE вернет значение -1
  if (res!=0) return -1; else return 0;
  //Если обратная матрица найдена, то функция INVERSE вернет значение 0, а
  //обратная матрица будет возвращаться через указатель double **y

}

 int main(){
   int result,i,j,N;
   //Двойные указатели для хранения исходной (**a) и обратной (**b) матрицы
   double **a,**b;
   printf("N=");
   scanf("%d",&N);
   //Выделение памяти для матриц a и b
   a=new double *[N];
   for(i=0;i<N;i++)
     a[i]=new double[N];
   b=new double *[N];
   for(i=0;i<N;i++)
     b[i]=new double[N];
   // Ввод исходной матрицы
   printf("Input Matrix A\n");
   for(i=0;i<N;i++)
     for(j=0;j<N;j++)
       scanf("%le",&a[i][j]);
   // вычисление обратной матрицы
   result = INVERSE(a,N,b);
   // Если обратная матрица существуют, то вывести ее на экран
   if (result==0){
     printf("Inverse matrix\n");
     for(i=0;i<N;printf("\n"),i++)
       for(j=0;j<N;j++)
         printf("%le\t",b[i][j]);
   }
   else
   // Если обратная матрица не существуют, то вывести сообщение
   printf("No Inverse marix\n");
   // Освобождение памяти
   for(i=0;i<N;i++)
     delete [] a[i];
   delete [] a;
   for(i=0;i<N;i++)
     delete [] b[i];
   delete [] b;
}
