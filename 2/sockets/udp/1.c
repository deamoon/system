#include "data.h"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

void cher(const char * Where) {
    if (errno) {
        perror(Where);
        exit(1);
    }
}

int main(int argc, char ** argv) {
    if (argc != 3) {
        fprintf(stderr, "I need IP and port\n");
        exit(1);
    }
    int s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (s == -1) {
        cher("socket");
    }
    struct sockaddr_in remote_addr;
    remote_addr.sin_family = AF_INET;
    remote_addr.sin_port = htons(atoi(argv[2]));
    remote_addr.sin_addr.s_addr = inet_addr(argv[1]);

    struct Data d;
    d.chislo = 3;
    d.eshe_chislo = 4;
    d.opyat_chislo = 3.14;
    d.massiv[0] = 1.1;
    d.massiv[1] = 2.2;
    d.massiv[2] = 3.3;
    d.massiv[3] = 4.4;

    if (sendto(
            s,
            &d,
            sizeof(struct Data),
            0,
            (struct sockaddr *)&remote_addr,
            sizeof(struct sockaddr_in)) == -1)
    {
        cher("sendto");
    }

    close(s);
    return 0;
}
