#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include "data.h"

void cher(const char * Where) {
    if (errno) {
        perror(Where);
        exit(1);
    }
}

int main(int argc, char ** argv) {
    int s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (s == -1) {
        cher("socket");
    }
    struct sockaddr_in local_addr;
    local_addr.sin_family = AF_INET;
    local_addr.sin_port = htons(4321);
    local_addr.sin_addr.s_addr = INADDR_ANY;
    if (bind(s, (struct sockaddr *)&local_addr, sizeof(struct sockaddr_in)) == -1) {
        cher("bind");
    }
    
    struct Data d;
    struct sockaddr_in remote_addr;
    socklen_t addr_len = sizeof(struct sockaddr_in);
    
    while (1) {
        if (recvfrom(
                s,
                &d,
                sizeof(struct Data),
                0,
                (struct sockaddr *)&remote_addr,
                &addr_len) == -1)
        {
            cher("recvfrom");
        }
        printf("%d\n", d.chislo);
        printf("%d\n", d.eshe_chislo);
        printf("%lf\n", d.opyat_chislo);
        printf("%f\n", d.massiv[0]);
        printf("%f\n", d.massiv[1]);
        printf("%f\n", d.massiv[2]);
        printf("%f\n", d.massiv[3]);
    }
    
    close(s);
    return 0;
}
