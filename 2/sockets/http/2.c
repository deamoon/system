#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <dirent.h>
#include <string.h>
#include <memory.h>

#define BUFF_LENGTH 16

void cher(const char * Where) {
    if (errno) {
        perror(Where);
        exit(1);
    }
}

struct Buff {
    char * data;
    int buff_length;
    int size;
    int position;
};

void make_buff(struct Buff * buff, int length) {
    buff->data = (char *)malloc(sizeof(char) * length);
    buff->buff_length = length;
    buff->size = 0;
    buff->position = 0;
}

void extend_buff(struct Buff * buff) {
    char * new_data = (char *)malloc(sizeof(char) * buff->buff_length * 2);
    int i;
    for (i = 0; i < buff->size; ++i) {
        new_data[i] = buff->data[i];
    }
    free(buff->data);
    buff->data = new_data;
    buff->buff_length *= 2;
}

void free_buff(struct Buff * buff) {
    free(buff->data);
    buff->data = NULL;
    buff->buff_length = 0;
    buff->size = 0;
    buff->position = 0;
}

int get_char(int fd, struct Buff * buff) {
    if (buff->position == buff->size) {
        buff->position = 0;
        buff->size = 0;
        int r = read(fd, buff->data, buff->buff_length);
        if (r > 0) {
            buff->size = r;
        }
    }
    if (buff->position == buff->size) {
        return EOF;
    }
    return buff->data[buff->position++];
}

struct Buff read_to(int fd, struct Buff * buff, char * sep) {
    const int sep_length = strlen(sep);
    struct Buff res;
    make_buff(&res, BUFF_LENGTH);
    while (1) {
        if (res.size == res.buff_length) {
            extend_buff(&res);
        }
        int c = get_char(fd, buff);
        if (c == EOF) {
            break;
        }
        res.data[res.size++] = (char)c;
        if (res.size < sep_length) {
            continue;
        }
        if (!strncmp(sep, &res.data[res.size - sep_length], sep_length)) {
            res.size -= sep_length;
            break;
        }
    }
    return res;
}

const char * const OK = "\
HTTP/1.1 200 Ok\r\n\
Connection: close\r\n\
Accept-Ranges:  none\r\n\
Content-Type: text/html; charset=UTF-8\r\n\r\n";

const char * const BEGIN = "<html><body><h1>Simple http server</h1>";
const char * const END = "</body></html>\r\n";

void transfer_data(int fd) {
    struct Buff buff;
    make_buff(&buff, BUFF_LENGTH);
    struct Buff lines[16];
    int line_count = 0;
    while (1) {
        struct Buff line = read_to(fd, &buff, "\r\n");
        if (line.size == 0) {
            free_buff(&line);
            break;
        }
        if (line_count >= 16) {
            free_buff(&line);
            continue;
        }
        lines[line_count++] = line;
    }

    write(fd, OK, strlen(OK));
    write(fd, BEGIN, strlen(BEGIN));
    int i;
    for (i = 0; i < line_count; ++i) {
        write(fd, "<h3>", 4);
        write(fd, lines[i].data, lines[i].size);
        write(fd, "</h3>", 5);
    }
    write(fd, END, strlen(END));

    for (i = 0; i < line_count; ++i) {
        free_buff(&lines[i]);
    }
    free_buff(&buff);
}

int main() {
    int s = socket(AF_INET, SOCK_STREAM, 0);
    if (s == -1) {
        cher("socket");
    }
    struct sockaddr_in local_addr;
    local_addr.sin_family = AF_INET;
    local_addr.sin_port = htons(8081);
    local_addr.sin_addr.s_addr = inet_addr("0.0.0.0");

    int optval = 1;
    if (setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval)) == -1) {
        cher("setsockopt");
    }

    if (!bind(s, (const struct sockaddr *)&local_addr, sizeof(struct sockaddr_in))) {
        cher("bind");
    }
    while (!listen(s, 10)) {
        struct sockaddr_in remote_addr;
        socklen_t addr_len = sizeof(struct sockaddr_in);
        int rs = accept(s, (struct sockaddr *)&remote_addr, &addr_len);
        if (rs == -1) {
            cher("accept");
        }
        fprintf(stderr, "Connected: %s:%d\n", inet_ntoa(remote_addr.sin_addr), (int)ntohs(remote_addr.sin_port));
        transfer_data(rs);
        close(rs);
        fprintf(stderr, "Closed: %s:%d\n", inet_ntoa(remote_addr.sin_addr), (int)ntohs(remote_addr.sin_port));
    }
    cher("listen");
    return 0;
}
