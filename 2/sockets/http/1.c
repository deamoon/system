#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <dirent.h>
#include <string.h>
#include <memory.h>

const char * const OK = "\
HTTP/1.1 200 Ok\r\n\
Connection: close\r\n\
Accept-Ranges:  none\r\n\
Content-Type: text/html; charset=UTF-8\r\n\r\n";

const char * const BEGIN = "<html><body><h1>Simple http server</h1><table>";
const char * const END = "</table></body></html>\r\n";
const char * const b = "<tr><td>";
const char * const e = "</td></tr>";

char * itoa(int val, int base) {
    if (val == 0) {
        return "0";
    }
    static char buf[32];
    memset(buf, 0, 32);

    int i = 30;
    for(; val && i; --i, val /= base)
        buf[i] = "0123456789abcdef"[val % base];
    return &buf[i + 1];
}

void cher(const char * Where) {
    if (errno) {
        perror(Where);
        exit(1);
    }
}

void transfer_data(int fd) {
    send(fd, OK, strlen(OK), 0);
    cher("Warning");
    send(fd, BEGIN, strlen(BEGIN), 0);
    cher("Warning");
    
    DIR * dir = opendir(".");
    struct dirent * ent;
    int index = 0;
    while ((ent = readdir(dir)) != NULL) {
        //char * i = itoa(index++, 10);
        char * n = ent->d_name;
        
        send(fd, b, strlen(b), 0);
        cher("Warning");
        //send(fd, i, strlen(i), 0);
        cher("Warning");
        //send(fd, " ", 1, 0);
        cher("Warning");
        send(fd, n, strlen(n), 0);
        cher("Warning");
        send(fd, e, strlen(e), 0);
        cher("Warning");
    }
    closedir(dir);
    
    send(fd, END, strlen(END), 0);
    cher("Warning");
}

int main() {
    int s = socket(AF_INET, SOCK_STREAM, 0);
    if (s == -1) {
        cher("socket");
    }
    struct sockaddr_in local_addr;
    local_addr.sin_family = AF_INET;
    local_addr.sin_port = htons(8084);
    local_addr.sin_addr.s_addr = inet_addr("0.0.0.0");
    if (!bind(s, (const struct sockaddr *)&local_addr, sizeof(struct sockaddr_in))) {
        cher("bind");
    }
    while (!listen(s, 100)) {
        struct sockaddr_in remote_addr;
        socklen_t addr_len = sizeof(struct sockaddr_in);
        int rs = accept(s, (struct sockaddr *)&remote_addr, &addr_len);
        if (rs == -1) {
            cher("accept");
        }
        fprintf(stderr, "Connected: %s:%d\n", inet_ntoa(remote_addr.sin_addr), (int)ntohs(remote_addr.sin_port));
        transfer_data(rs);
        close(rs);
        fprintf(stderr, "Closed: %s:%d\n", inet_ntoa(remote_addr.sin_addr), (int)ntohs(remote_addr.sin_port));
    }
    cher("listen");
    return 0;
}
