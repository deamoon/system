#include <netdb.h>
#include <errno.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>

void cher(char * Where) {
    if (errno) {
        perror(Where);
    }
}

int main(int argc, char ** argv) {
    if (argc <= 1) {
        fprintf(stderr, "Give me hosts");
        return 1;
    }
    int i;
    for (i = 1; i < argc; ++i) {
        char * host = argv[i];
        printf("Host: %s\n", host);
        struct addrinfo * h = NULL;
        if (getaddrinfo(host, NULL, NULL, &h) != 0) {
            cher("getaddrinfo");
            continue;
        }
        while (h != NULL) {
            if (h->ai_family == AF_INET) {
                struct sockaddr_in * s = (struct sockaddr_in *)h->ai_addr;
                printf("Addr: %s\n", inet_ntoa(s->sin_addr.s_addr));
            }
            h = h->ai_next;
        }
        freeaddrinfo(h);
    }
    return 0;
}








