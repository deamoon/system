#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

void cher(const char * Where) {
    if (errno) {
        perror(Where);
        exit(1);
    }
}

void transfer_data(int fd) {
    char buf[1024];
    int r;
    while ((r = read(fd, buf, 1024)) > 0) {
        write(1, buf, r);
    }
}

int main() {
    int s = socket(AF_INET, SOCK_STREAM, 0);
    if (s == -1) {
        cher("socket");
    }
    struct sockaddr_in local_addr;
    local_addr.sin_family = AF_INET;
    local_addr.sin_port = htons(1234);
    local_addr.sin_addr.s_addr = inet_addr("0.0.0.0");
    if (!bind(s, (const struct sockaddr *)&local_addr, sizeof(struct sockaddr_in))) {
        cher("bind");
    }
    while (!listen(s, 10)) {
        struct sockaddr_in remote_addr;
        socklen_t addr_len = sizeof(struct sockaddr_in);
        int rs = accept(s, (struct sockaddr *)&remote_addr, &addr_len);
        if (rs == -1) {
            cher("accept");
        }
        fprintf(stderr, "Connected: %s:%d\n", inet_ntoa(remote_addr.sin_addr), (int)ntohs(remote_addr.sin_port));
        transfer_data(rs);
        close(rs);
    }
    cher("listen");
    return 0;
}
