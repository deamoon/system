#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

void cher(const char * Where) {
    if (errno) {
        perror(Where);
        exit(1);
    }
}

void transfer_data(int fd) {
    char buf[1024];
    int r;
    while ((r = read(0, buf, 1024)) > 0) {
        write(fd, buf, r);
    }
}

int main(int argc, char ** argv) {
    if (argc != 3) {
        fprintf(stderr, "I need IP and port\n");
        exit(1);
    }
    int s = socket(AF_INET, SOCK_STREAM, 0);
    if (s == -1) {
        cher("socket");
    }
    struct sockaddr_in remote_addr;
    remote_addr.sin_family = AF_INET;
    remote_addr.sin_port = htons(atoi(argv[2]));
    remote_addr.sin_addr.s_addr = inet_addr(argv[1]);
    if (connect(s, (const struct sockaddr *)&remote_addr, sizeof(struct sockaddr_in))) {
        cher("connect");
    }
    transfer_data(s);
    return 0;
}
