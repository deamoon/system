#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

void cher(const char * Where) {
    if (errno) {
        perror(Where);
        exit(1);
    }
}

void transfer_data(int fd) {
    fcntl(0, F_SETFL, O_NONBLOCK);
    cher("fcntl(0, ...");
    fcntl(fd, F_SETFL, O_NONBLOCK);
    cher("fcntl(fd, ...");
    
    fd_set rfds;
    
    char buf[1024];
    int r;
    
    while (1) {
        FD_ZERO(&rfds);
        FD_SET(0, &rfds);
        FD_SET(fd, &rfds);
        
        select(fd + 1, &rfds, NULL, NULL, NULL);
        
        if (FD_ISSET(0, &rfds)) {
            r = read(0, buf, 1024);
            if (r <= 0) {
                cher("read(0, ...");
                break;
            }
            if (write(fd, buf, r) != r) {
                cher("write(fd, ...");
                break;
            }
        }
        if (FD_ISSET(fd, &rfds)) {
            r = read(fd, buf, 1024);
            if (r <= 0) {
                cher("read(fd, ...");
                break;
            }
            if (write(1, buf, r) != r) {
                cher("write(1, ...");
                break;
            }
        }
    }
}

int main(int argc, char ** argv) {
    if (argc != 3) {
        fprintf(stderr, "I need IP and port\n");
        exit(1);
    }
    int s = socket(AF_INET, SOCK_STREAM, 0);
    if (s == -1) {
        cher("socket");
    }
    struct sockaddr_in remote_addr;
    remote_addr.sin_family = AF_INET;
    remote_addr.sin_port = htons(atoi(argv[2]));
    remote_addr.sin_addr.s_addr = inet_addr(argv[1]);
    if (connect(s, (const struct sockaddr *)&remote_addr, sizeof(struct sockaddr_in))) {
        cher("connect");
    }
    transfer_data(s);
    return 0;
}
