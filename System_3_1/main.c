#include <stdio.h>
#include <stdlib.h>

// пустые, с пробелами, a"a"a

typedef struct myy_string my_string;

struct myy_string{
    int length;
    char *s;
};

void prov(char *s){
    if (!s) {
        fprintf(stderr,"Don't acceess memory");
        exit(1);
    }
}

void prov2(my_string *s){
    if (!s) {
        fprintf(stderr,"Don't acceess memory");
        exit(1);
    }
}

int bufer_vvod(my_string * s){
    s->s = (char *) malloc(sizeof(char)*(s->length)); prov(s->s);
    char c; int k = -1;
    while (1) {
        c = getchar();
        if (c==EOF) exit(0);
        if (k==s->length-1) {
            s->s = realloc(s->s, sizeof(char) * (s->length*=2)); prov(s->s);
        }
        if (c=='\n') {
            s->s[++k] = '\0';
            break;
        } else
            s->s[++k] = c;
    }
    s->length = k;
    return 0;
}

void improve(my_string **arv, int *arg){
    if (*arv) {
        (*arv) = (my_string *) realloc((*arv), sizeof(my_string) * (*arg) * 2); prov2((*arv));
        for (int i = (*arg); i < 2 * (*arg); ++i) {
            (*arv)[i].length = 8;
            (*arv)[i].s = (char *) malloc(sizeof(char) * ((*arv)[i].length + 1)); prov((*arv)[i].s);
        }
        (*arg) = (*arg) * 2;
    } else {
        (*arg) = 8;
        (*arv) = (my_string *) malloc(sizeof(my_string) * (*arg));
        prov2((*arv));
        for (int i=0; i<(*arg); ++i) {
            (*arv)[i].length = 8;
            (*arv)[i].s = (char *) malloc(sizeof(char)*((*arv)[i].length + 1)); prov((*arv)[i].s);
        }
    }
}

void improve2(my_string * s){
    s->s = realloc(s->s, sizeof(char) * ((s->length*=2) + 1)); prov(s->s);
}

int main() {
    while (1) {
        my_string s1 = {8, NULL};
        if (bufer_vvod(&s1)) exit(1);
        char *s =  s1.s;
        int n = s1.length, arg = 0;

        my_string *arv = NULL;
        improve(&arv, &arg);

        int q1 = 0; // мы не в двойных ковычках
        int q2 = 0; // мы не в слове
        int k = -1, j = -1, kav = 0; //j - номер слова, k - буква в слове arv[j]
        for (int i=0; i<=n; ++i) {
            if (s[i]=='"') ++kav;
            if (((s[i]=='"') && (!q1)) || (s[i-1]=='"') && (s[i]=='"')) {
                if (q2){
                    arv[j].length = k+1;
                    arv[j].s[++k] = '\0';
                    q2 = 0;
                }
                q1 = 1;
                if ((s[i-1]=='"') && (s[i]=='"')) q1=0;
                continue;
            } // зашли в ковычки
            if ((s[i]=='"') && (q1)) {
                arv[j].length = k+1;
                arv[j].s[++k] = '\0';
                q1 = 0; q2 = 0; continue;
            } // вышли из ковычек
            if (q1) {
                if (!q2) {
                    q2 = 1; k = -1;
                    if (++j == arg) improve(&arv, &arg);
                }
                if (++k == arv[j].length) improve2(&arv[j]);
                arv[j].s[k] = s[i];
            } else {
                if ((s[i]==' ') || (s[i]=='\t') || (s[i]=='\0')) {
                    if (q2) {
                        arv[j].length = k+1;
                        arv[j].s[++k] = '\0';
                        q2 = 0;
                    }
                } else {
                    if (!q2) {
                        q2 = 1; k = -1;
                        if (++j == arg) improve(&arv, &arg);
                    }
                    if (++k == arv[j].length) improve2(&arv[j]);
                    arv[j].s[k] = s[i];
                }
            }
        }

        if ((q1) || (kav % 2 == 1)) {
            printf("Kavichka is out\n");
        } else {
            printf("%d\n",j+1); // Кол-во слов
            for (int i=0;i<j+1;++i){
                printf("%d '",arv[i].length); // Длина слова
                for (int i1=0;i1<arv[i].length;++i1) {
                    printf("%c",arv[i].s[i1]);
                }
                printf("'\n");
            }
        }

        free(s);
        for (int i=0;i<arg;++i) free(arv[i].s);
        free(arv);
    }
    return 0;
}
