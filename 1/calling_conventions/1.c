int __stdcall stdcall(int arg1, int arg2) {
    return arg1 ^ arg2;
}

int __cdecl cdecl(int arg1, int arg2) {
    return arg1 ^ arg2;
}

int __fastcall fastcall(int arg1, int arg2) {
    return arg1 ^ arg2;
}

void F() {
    cdecl(3, 4);
    stdcall(1, 2);
    fastcall(5, 6);
}
