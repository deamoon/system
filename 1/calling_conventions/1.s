	.file	"1.c"
	.text
.globl _stdcall@8
	.def	_stdcall@8;	.scl	2;	.type	32;	.endef
_stdcall@8:
	pushl	%ebp
	movl	%esp, %ebp
	movl	12(%ebp), %eax
	movl	8(%ebp), %edx
	xorl	%edx, %eax
	popl	%ebp
	ret	$8
.globl _cdecl
	.def	_cdecl;	.scl	2;	.type	32;	.endef
_cdecl:
	pushl	%ebp
	movl	%esp, %ebp
	movl	12(%ebp), %eax
	movl	8(%ebp), %edx
	xorl	%edx, %eax
	popl	%ebp
	ret
.globl @fastcall@8
	.def	@fastcall@8;	.scl	2;	.type	32;	.endef
@fastcall@8:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$8, %esp
	movl	%ecx, -4(%ebp)
	movl	%edx, -8(%ebp)
	movl	-8(%ebp), %eax
	movl	-4(%ebp), %edx
	xorl	%edx, %eax
	leave
	ret
.globl _F
	.def	_F;	.scl	2;	.type	32;	.endef
_F:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$8, %esp
	movl	$4, 4(%esp)
	movl	$3, (%esp)
	call	_cdecl
	movl	$2, 4(%esp)
	movl	$1, (%esp)
	call	_stdcall@8
	subl	$8, %esp
	movl	$6, %edx
	movl	$5, %ecx
	call	@fastcall@8
	leave
	ret
