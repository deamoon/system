#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

void cher(char * str) {
    if (errno) {
        perror(str);
        exit(1);
    }
}

int main(void) {
    int child_stdin_pipe[2];
    int child_stdout_pipe[2];
    
    pipe(child_stdin_pipe); cher("first pipe");
    pipe(child_stdout_pipe); cher("second pipe");
    
    pid_t c1;
    if ((c1 = fork()) == 0) {
        //Child
        cher("first fork");
        close(child_stdin_pipe[1]);
        close(child_stdout_pipe[0]);
        
        dup2(child_stdin_pipe[0], 0); cher("first dup");
        dup2(child_stdout_pipe[1], 1); cher("second dup");
        
        int c;
        while ((c = getchar()) != EOF) {
            cher("child getchar");
            printf("< %d >\n", c); cher("child printf");
        }
        
        return 0;
    } else {
        //Parent
        close(child_stdin_pipe[0]);
        close(child_stdout_pipe[1]);
        
        pid_t c2;
        if ((c2 = fork()) == 0) {
            //Read
            cher("second fork");
            close(child_stdin_pipe[1]);
            void * buf = malloc(sizeof(char) * 8);
            ssize_t b;
            while ((b = read(child_stdout_pipe[0], buf, 8)) > 0) {
                cher("reading process read");
                write(1, buf, b); cher("reading process write");
            }
            free(buf);
            close(child_stdout_pipe[0]);
            
            return 0;
        } else {
            //Write
            close(child_stdout_pipe[0]);
            void * buf = malloc(sizeof(char) * 8);
            ssize_t b;
            while ((b = read(0, buf, 8)) > 0) {
                cher("main process read");
                write(child_stdin_pipe[1], buf, b); cher("main process write");
            }
            free(buf);
            close(child_stdin_pipe[1]);
            waitpid(c2, NULL, 0); cher("wait for reading process");
        }
        
        waitpid(c1, NULL, 0); cher("wait for child");
    }
    return 0;
}
