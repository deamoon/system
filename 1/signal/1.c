#include <signal.h>
#include <stdio.h>

void handler(int s) {
    printf("Signal!!!\n");
}

int main(void) {
    signal(SIGUSR1, handler);
    printf("Ready\n %d",SIGUSR1);
    for (;;) { }
}
