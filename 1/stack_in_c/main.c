#include <stdio.h>
#include <stdlib.h>

int * test_function(int p1, int p2, int * mem) {
    int temp = (int)&temp;

    mem[0] = (&temp)[-10];
    mem[1] = (&temp)[-9];
    mem[2] = (&temp)[-8];
    mem[3] = (&temp)[-7];
    mem[4] = (&temp)[-6];
    mem[5] = (&temp)[-5];
    mem[6] = (&temp)[-4];
    mem[7] = (&temp)[-3];
    mem[8] = (&temp)[-2];
    mem[9] = (&temp)[-1];
    mem[10] = (&temp)[0];
    mem[11] = (&temp)[1];
    mem[12] = (&temp)[2];
    mem[13] = (&temp)[3];
    mem[14] = (&temp)[4];
    mem[15] = (&temp)[5];
    mem[16] = (&temp)[6];
    mem[17] = (&temp)[7];
    mem[18] = (&temp)[8];
    mem[19] = (&temp)[9];
    mem[20] = (&temp)[10];

    return (&temp);
}

void exit_function() {
    printf("OLOLO\n");
    exit(1);
}

void ololo(int addr) {
    int * temp = (int *)&temp;
    *(temp + 2) = addr;
}

int main(int argc, char ** argv) {
    printf("I am going to call %d:\n", (int)&test_function);
    int * mem = (int *)malloc(sizeof(int) * 21);
    int * ptemp = (int *)303030;
    ptemp = test_function(101010, 202020, mem);
    int i;
    for (i = -10; i <= 10; ++i) {
        printf("%d+(%d) = %d\n", (int)ptemp, (int)i, mem[i + 10]);
    }

    ololo((int)&exit_function);

    printf("TROLOLO");

    return 0;
}
