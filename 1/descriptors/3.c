#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>

void cher(const char * Where) {
    if (errno) {
        perror(Where);
        exit(1);
    }
}

int main(void) {
    fcntl(0, F_SETFL, O_NONBLOCK);
    cher("fcntl");
    
    fd_set rfds;
    FD_ZERO(&rfds);
    FD_SET(0, &rfds);
    
    char buf[1024];
    int rd;
    while (1) {
        rd = select(1, &rfds, NULL, NULL, NULL);
        cher("select");
        
        if (FD_ISSET(0, &rfds)) {
            rd = read(0, buf, 1024);
            cher("read");
        
            if (rd == 0) {
                break;
            }
        
            write(1, buf, rd);
            cher("write");
        }
    }
    return 0;
}
