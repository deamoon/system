#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>

void cher(const char * Where) {
    if (errno) {
        perror(Where);
        exit(1);
    }
}

int main(void) {
    char buf[1024];
    int rd;
    while ((rd = read(0, buf, 1024)) > 0) {
        write(1, buf, rd);
        cher("write");
    }
    cher("read");
    return 0;
}
