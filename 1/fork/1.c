#include <stdio.h>
#include <unistd.h>

int main() {
    int pid = fork();
    if (pid < 0) {
        printf("Error\n");
    } else if (pid == 0) {
        printf("Child\n");
    } else {
        sleep(2);
        printf("Parent\n");
    }
}