#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

int main() {
    int pid = fork();
    if (pid < 0) {
        printf("Error\n");
    } else if (pid == 0) {
        sleep(2);
        printf("Child\n");
        execlp("ls", "/bin/ls", "-lh", (char *)0);
        perror(NULL);
    } else {
        printf("Parent\n");
        wait(NULL);
    }
}
