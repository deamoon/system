#include <stdio.h>
#include <unistd.h>

int main() {
    int pid = fork();
    if (pid < 0) {
        printf("Error\n");
    } else if (pid == 0) {
        sleep(2);
        printf("Child\n");
    } else {
        printf("Parent\n");
        wait(NULL);
    }
}
