#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#define NUM_THREADS     5

volatile int A = 0;

void * PrintHello(void * param)
{
    const int d = (const int)param;
    int i;
    for (i = 0; i < 100000; ++i) {
        A += d;
    }
}

int main (int argc, char *argv[])
{
    pthread_t threads[NUM_THREADS];
    int rc;
    int t;
    for (t = 0; t < NUM_THREADS; ++t) {
       rc = pthread_create(&threads[t], NULL, PrintHello, (void *)1);
       if (rc) {
          printf("ERROR; return code from pthread_create() is %d\n", rc);
          exit(-1);
       }
    }
    for (t = 0; t < NUM_THREADS; ++t) {
        pthread_join(threads[t], NULL);
    }
    printf("%d", A);
}
