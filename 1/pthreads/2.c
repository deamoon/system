#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#define NUM_THREADS     5

void * PrintHello(void * threadid)
{
    int tid = (int)threadid;
    int i;
    
    for (i = 0; i < 10; ++i) {
        printf("Hello World! It's me, thread #%d!. Step #%d\n", tid, i);
        sleep(1);
    }

    pthread_exit(NULL);
}

int main (int argc, char *argv[])
{
    pthread_t threads[NUM_THREADS];
    int rc;
    int t;
    for(t = 0; t < NUM_THREADS; ++t) {
       printf("In main: creating thread %d\n", t);
       rc = pthread_create(&threads[t], NULL, PrintHello, (void *)t);
       if (rc) {
          printf("ERROR; return code from pthread_create() is %d\n", rc);
          exit(-1);
       }
    }
}
