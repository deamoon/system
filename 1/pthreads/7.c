#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

const char * mem_name = "my_little_memory";
int memd;
int size = sizeof(pthread_mutex_t);

void cher(const char * where) {
    if (errno) {
        perror(where);
        exit(1);
    }
}

void Loop() {
    memd = shm_open(mem_name, O_RDWR | O_CREAT, 0666);
    if (memd == -1) {
        cher("shm_open");
    }
    pthread_mutex_t * mut = NULL;
    mut = mmap(NULL, size, PROT_WRITE | PROT_READ, MAP_SHARED, memd, 0);
    if (mut == MAP_FAILED) {
        cher("child mmap");
    }
    close(memd);
    if (pthread_mutex_lock(mut)) {
        cher("child pthread_mutex_lock");
    }
    fprintf(stderr, "Loop: Wassup my nigga bro\n");
    if (pthread_mutex_unlock(mut)) {
        cher("child pthread_mutex_unlock");
    }
    if (munmap(mut, size)) {
        cher("child munmap");
    }
}

void create_mutex(pthread_mutex_t * m) {
    pthread_mutexattr_t attr;
    if (pthread_mutexattr_init(&attr)) {
        cher("pthread_mutexattr_init");
    }
    if (pthread_mutexattr_setpshared(&attr, PTHREAD_PROCESS_SHARED)) {
        cher("pthread_mutexattr_setpshared");
    }
    if (pthread_mutex_init(m, &attr)) {
        cher("pthread_mutex_init");
    }
    if (pthread_mutexattr_destroy(&attr)) {
        cher("pthread_mutexattr_destroy");
    }
}

int main (int argc, char *argv[])
{
    int i;
    pthread_mutex_t * mut = NULL;
    memd = shm_open(mem_name, O_RDWR | O_CREAT, 0666);
    if (memd == -1) {
        cher("shm_open");
    }
    if (ftruncate(memd, size)) {
        cher("ftruncate");
    }
    mut = mmap(NULL, size, PROT_WRITE | PROT_READ, MAP_SHARED, memd, 0);
    if (mut == MAP_FAILED) {
        cher("parent mmap");
    }
    close(memd);
    create_mutex(mut);
    
    if (pthread_mutex_lock(mut)) {
        cher("parent pthread_mutex_lock");
    }
    for (i = 0; i < 10; ++i) {
        if (!fork()) {
            Loop();
            exit(0);
        }
    }
    getchar();
    if (pthread_mutex_unlock(mut)) {
        cher("parent pthread_mutex_unlock");
    }
    for (i = 0; i < 10; ++i) {
        wait(NULL);
    }
    if (pthread_mutex_destroy(mut)) {
        cher("parent pthread_mutex_destroy");
    }
    if (munmap(mut, size)) {
        cher("parent munmap");
    }
    if (shm_unlink(mem_name)) {
        cher("shm_unlink");
    }
}
