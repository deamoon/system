#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#define NUM_THREADS     5

int A = 0;
pthread_mutex_t m;

void * PrintHello(void * param)
{
    const int d = (const int)param;
    int i;
    for (i = 0; i < 100000; ++i) {
        pthread_mutex_lock(&m);
        A += d;
        pthread_mutex_unlock(&m);
    }
}

int main (int argc, char *argv[])
{
    pthread_t threads[NUM_THREADS];
    int rc;
    int t;
    
    pthread_mutex_init(&m, NULL);
    
    for (t = 0; t < NUM_THREADS; ++t) {
       rc = pthread_create(&threads[t], NULL, PrintHello, (void *)1);
       if (rc) {
          printf("ERROR; return code from pthread_create() is %d\n", rc);
          exit(-1);
       }
    }
    for (t = 0; t < NUM_THREADS; ++t) {
        pthread_join(threads[t], NULL);
    }
    pthread_mutex_destroy(&m);
    printf("%d", A);
}
