#include <fcntl.h>
#include <semaphore.h>
#include <errno.h>

const char * sem_name = "my_little_semaphore";

int main() {
    sem_t * s = sem_open(sem_name, O_CREAT | O_EXCL, 0666, 5);
    perror("sem_open");
    return 0;
}