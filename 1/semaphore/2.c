#include <fcntl.h>
#include <semaphore.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

const char * sem_name = "my_little_semaphore";

void cher(const char * where) {
    if (errno) {
        perror(where);
        exit(1);
    }
}

void child() {
    sem_t * s = sem_open(sem_name, O_CREAT, 0666, 5);
    if (s == SEM_FAILED) {
        cher("sem_open");
    }
    
    printf("Waiting in %d\n", getpid());
    if (sem_wait(s)) {
        cher("sem_wait");
    }
    printf("Waiting OK in %d\n", getpid());
    
    printf("Sleeping in %d\n", getpid());
    sleep(1);
    printf("Sleeping OK in %d\n", getpid());
    
    printf("Posting in %d\n", getpid());
    sem_post(s);
    printf("Posting OK in %d\n", getpid());
    
    if (sem_close(s)) {
        cher("sem_close");
    }
    
    exit(0);
}

int main() {
    sem_t * s = sem_open(sem_name, O_CREAT, 0666, 5);
    int i;
    for (i = 0; i < 10; ++i) {
        if (!fork()) {
            child();
        }
    }
    for (i = 0; i < 10; ++i) {
        wait(NULL);
    }
    if (sem_unlink(sem_name)) {
        cher("sem_unlink");
    }
    return 0;
}