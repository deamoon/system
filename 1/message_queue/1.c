#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <mqueue.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>

const char * queue_name = "my_queue3";

mqd_t qd;

void cher(const char * where)
{
    if (errno) {
        fprintf(stderr, "%d: %s: %d\n", getpid(), where, strerror(errno));
        exit(1);
    }
}

void parent() {    
    int ch;
    while (1) {
        ch = getchar();
        if (mq_send(qd, (const char *)&ch, sizeof(int), 0)) {
            cher("mq_send");
        }
        if (ch == EOF) {
            break;
        }
    }
}

void child() {
    int ch;
    unsigned pr;
    while (1) {
        if (mq_receive(qd, (char *)&ch, sizeof(int), &pr) == -1) {
            cher("mq_receive");
        }
        if (ch == EOF) {
            break;
        }
        putchar(ch);
    }
}

int main() {
    struct mq_attr ma;
    ma.mq_flags = 0;
    ma.mq_maxmsg = 16;
    ma.mq_msgsize = sizeof(int);
    qd = mq_open(queue_name, O_RDWR | O_CREAT, 0666, &ma);
    if (qd == (mqd_t)-1) {
        cher("mq_open");
    }
    
    if (fork()) {
        parent();
    } else {
        child();
        exit(0);
    }
    
    wait(NULL);
    
    if (mq_close(qd)) {
        cher("mq_close");
    }
    if (mq_unlink(queue_name)) {
        cher("mq_unlink");
    }
    return 0;
}


