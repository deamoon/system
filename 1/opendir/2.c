#include <glob.h>
#include <stdio.h>

int main(int argc, char ** argv) {
    if (argc == 1) {
        fprintf(stderr, "Give me a path");
        return 1;
    }
    int i;
    for (i = 1; i < argc; ++i) {
        glob_t globbuf;
        globbuf.gl_offs = 0;
        glob(argv[i], 0, NULL, &globbuf);
        size_t j;
        for (j = 0; j < globbuf.gl_pathc; ++j) {
            printf("%s\n", globbuf.gl_pathv[j]);
        }
        globfree(&globbuf);
    }
    return 0;
}