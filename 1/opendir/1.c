#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <stdio.h>

int main(void) {
    DIR * dir = opendir(".");
    struct dirent * ent;
    while ((ent = readdir(dir)) != NULL) {
        if (ent->d_type == DT_DIR) {
            printf("<%s>\n", ent->d_name);
        } else {
            struct stat s;
            stat(ent->d_name, &s);
            printf("[%s] - %d bytes\n", ent->d_name, (int)s.st_size);
        }
    }
    closedir(dir);
    return 0;
}
