#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>

void cher(const char * where) {
    if (errno) {
        perror(where);
        exit(1);
    }
}

int main(int argc, char *argv[])
{
    char * addr;
    int fd;
    struct stat sb;
    ssize_t s;
    
    if (argc != 2) {
        fprintf(stderr, "%s file\n", argv[0]);
        exit(1);
    }
    
    fd = open(argv[1], O_RDONLY);
    if (fd == -1) {
        cher("open");
    }
    
    if (fstat(fd, &sb) == -1) {
        cher("fstat");
    }
    
    addr = mmap(NULL, sb.st_size, PROT_READ, MAP_PRIVATE, fd, 0);

    if (addr == MAP_FAILED) {
        cher("mmap");
    }
    
    s = write(STDOUT_FILENO, addr, sb.st_size);
    
    if (s != sb.st_size) {
        cher("write");
    }
    
    if (munmap(addr, sb.st_size)) {
        cher("munmap");
    }
    return 0;
}
