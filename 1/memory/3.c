#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

const char * mem_name = "my_little_memory";
const int size = 64;

void cher(const char * where) {
    if (errno) {
        perror(where);
        exit(1);
    }
}

int memd;

int parent() {
    char * addr = mmap(NULL, size, PROT_WRITE, MAP_SHARED, memd, 0);
    if (addr == MAP_FAILED) {
        return -1;
    }
    int i;
    for (i = 0; i < size; ++i) {
        addr[i] = (char)i;
    }
    if (munmap(addr, size)) {
        return -1;
    }
}

void child() {
    char * addr = mmap(NULL, size, PROT_READ, MAP_SHARED, memd, 0);
    if (addr == MAP_FAILED) {
        cher("mmap");
    }
    int i;
    for (i = 0; i < size; ++i) {
        printf("%d\n", (int)addr[i]);
    }
    if (munmap(addr, size)) {
        cher("munmap");
    }
}

int real_main() {
    if (ftruncate(memd, size)) {
        return -1;
    }
    
    int ret;
    if (ret = parent()) {
        return ret;
    }
    if (fork()) {
        wait(NULL);
    } else {
        child();
        exit(0);
    }
    return 0;
}

int main(int argc, char *argv[]) {
    memd = shm_open(mem_name, O_RDWR | O_CREAT, 0666);
    if (memd == -1) {
        cher("shm_open");
    }
    
    if (real_main()) {
        perror("real_main");
    }
    
    if (shm_unlink(mem_name)) {
        cher("shm_open");
    }
    return 0;
}
