#include <stdio.h>
#include <errno.h>

int main(int argc, char ** argv) {
    if (argc != 2) {
        printf("Give me a file\n");
        return 1;
    }

    FILE * f = fopen(argv[1], "r");
    printf("Errno = %d\n", errno);
    printf("Error opening %s: %s\n", argv[1], strerror(errno));
    perror("OLOLO");

    if (f != NULL) {
	    fclose(f);
	}
}