#include "a.h"
#include "b.h"
#include "c.h"

#include <stdio.h>

int main(int argc, char ** argv) {
    a();
    b();
    c();
}