#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <signal.h>
#include <pthread.h>
typedef struct myy_string my_string;
struct myy_string{
    int length;
    char *s;
};

int prov(char *s){
    if (!s) {
        fprintf(stderr,"Don't acceess memory");
        return -2;
    }
    return 0;
}

int line_vvod(my_string * s){
    s->length = 8; if (s->s) free(s->s);
    s->s = (char *) malloc(sizeof(char)*(s->length)); if (prov(s->s)) return -2;
    char c; int k = -1;
    while (1) {
        c = getchar();
        //if ((c==EOF) && (k == -1)) exit(0);
        if (k==s->length-1) {
            s->s = realloc(s->s, sizeof(char) * (s->length*=2)); if (prov(s->s)) return -2;
        }
        if ( (c=='\n') || (c==EOF) ) {
            s->s[++k] = '\0';
            break;
        } else
            s->s[++k] = c;
    }
    s->length = k;
    return 0;
}

char * bash[]=
    {
        "/bin/bash",
        "-c",
        "cat",
        NULL
    };
my_string *arv = NULL;

void run(int n) {
    bash[2] = arv[n].s;
    execvp("/bin/bash",bash);
}

pid_t pid[100];
int pip[100][2], pip_err[100][2], pip_res[100] = {-1};
pthread_mutex_t m;

void * PipeErr(void * k) {
    int i = (int) k;
    char buf[1024]; int size;

    pthread_mutex_lock(&m);
      dup2(pip_err[i][0],0); close(pip_err[i][0]);
      while (pip_res[i]==-1) {
      fprintf(stderr,"\n -- %s -- \n", arv[i].s);

        while (size = read(0,buf,1024)) {
            write(2,buf,size);
        }
      fprintf(stderr," -- %s -- \n", arv[i].s);
      }
      fprintf(stderr,"\n -- %s -- \n", arv[i].s);
        while (size = read(0,buf,1024)) {
            write(2,buf,size);
        }
      fprintf(stderr," -- %s -- \n", arv[i].s);
    pthread_mutex_unlock(&m);

    pthread_exit(NULL);
}

int main(){
  freopen("1.txt","r",stdin);
  int i, n, N, j;
  scanf("%d",&N); getchar(); // считали \n
  arv = (my_string *) malloc(sizeof(my_string) * (N+1));
  for (i=1;i<=N;++i) {
      pipe(pip[i]); pipe(pip_err[i]);
      if (line_vvod(&arv[i])) exit(1);
  }

  if (N==1) { run(1); }

  int st;
  pid_t pid_res;
  for (n=1; n<=N; ++n) {
      if ((pid[n] = fork()) == 0) {
      //Child
          //pip_res = 0;
          dup2(pip_err[n][1],2); close(pip_err[n][0]); close(pip_err[n][1]);

          if  (n == N) {
              dup2(pip[n-1][0], 0);  close(pip[n-1][1]);  close(pip[n-1][0]);
          } else if (n == 1){
              dup2(pip[n][1], 1);  close(pip[n][1]);  close(pip[n][0]);
          } else {
              dup2(pip[n][1], 1);  close(pip[n][1]);  close(pip[n][0]);
              dup2(pip[n-1][0], 0);  close(pip[n-1][1]);  close(pip[n-1][0]);
          }
          run(n); // Запуск n-ой программы

      } else {
      //Parent
          //pip_res = 1;
          close(pip[n][1]); close(pip_err[n][1]);
      }
  }
//fprintf(stderr,"sd");

  pthread_t threads[100]; int rc; pthread_mutex_init(&m,NULL);
  for(i=1;i<=N;++i) {
    rc = pthread_create(&threads[i], NULL, PipeErr, (void *)i);
    if (rc) {
      printf("Error %d\n",rc);
      exit(-1);
    }
  }
  for(i=1;i<=N;++i) pthread_join(threads[i], NULL);


  for (i=1;i<=N;++i) {
      pid_res = waitpid(pid[i], &st, 0);
      if (WIFEXITED(st)) {
          pip_res[i] = 0;
          /*fprintf(2,"\n -- %s -- \n", arv[i].s);
            dup2(pip_err[i][0],0); close(pip_err[i][0]);
            char buf[1024]; int size;
            while (size = read(0,buf,1024)) {
                write(2,buf,size);
            }
          fprintf(2,"\n -- %s -- \n", arv[i].s);*/
      }
      else {
          pip_res[i] = 1;
          for (j=i+1;j<=n;++j) {
              pip_res[j] = 0;
              kill(pid[j], -9);
              printf("\n%s Убит\n", arv[i].s);
          }
      }
  }

  pthread_mutex_destroy(&m);
}
